<?php

namespace App\Http\Controllers\MasterUser;

use Auth;
use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\TmPemilik;
use App\Models\UserDetail;
use App\Models\UserTangselPay;
use Illuminate\Support\Carbon;

class UserController extends Controller
{
    protected $route = 'master-user.user.';
    protected $view  = 'pages.masterUser.';
    protected $title = 'User';
    protected $path  = 'profile/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik = TmPemilik::select('id', 'nama')->get();
        $time  = Carbon::now();
        $today = $time->format('Y-m');

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik',
            'today'
        ));
    }

    public function api(Request $request)
    {
        $jenis_kelamin = $request->jenis_kelamin;
        $tgl_daftar = $request->tgl_daftar;
        $tahun = \substr($tgl_daftar, 0, 4);
        $bulan = \substr($tgl_daftar, 5, 6);
        $nama = $request->nama;
        $pemilik = Auth::user()->tmpemilik_id;

        $user = UserTangselPay::queryTable($jenis_kelamin, $bulan,$tahun, $nama, $pemilik);

        return Datatables::of($user)
            ->addColumn('action', function ($p) {
                return "<a href='#' onclick='remove(" . $p->userDetail->id . ")' class='text-danger' title='Hapus Role'><i class='icon-remove'></i></a>";
            })
            ->editColumn('user_id', function ($p) {
                return $p->pemilik->nama;
            })
            ->editColumn('full_name', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->userDetail->id) . "' class='text-primary ' title='Show Data'>" . $p->userDetail->full_name . "</a>";
            })
            ->editColumn('email', function ($p) {
                return $p->userDetail->email;
            })
            ->editColumn('telp', function ($p) {
                return $p->userDetail->telp;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'user_id', 'full_name'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $userDetail = UserDetail::find($id);
        $user = UserTangselPay::where('id', $userDetail->user_id)->first();
        $pemilik = TmPemilik::where('id', $user->tmpemilik_id)->first();

        return view($this->view . 'show', compact(
            'route',
            'title',
            'path',
            'userDetail',
            'user',
            'pemilik'
        ));
    }

    public function destroy($id)
    {
        $data = UserDetail::find($id);

        // delete from table user
        UserTangselPay::where('id', $data->user_id)->delete();

        // delete from table user-detail
        UserDetail::destroy($id);

        return response()->json([
            'message' => 'Data user berhasil dihapus.'
        ]);
    }
}
