<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Support\Facades\DB;

// Model
use App\Models\Payment;
use App\Models\SubPayment;
use App\Models\TmMerchant;
use App\Models\TmTransaksi;
use App\Models\UserTangselPay;
use App\Models\TmMerchant_TmPemilik;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        /**
         * Check Role
         */
        $check = Auth::user()->model_has_role->first();
        $role  = Role::select('id', 'name')->where('id', $check->role_id)->first();

        /**
         * Count Merhcant
         */
        $tmmerchants = TmMerchant::select('tmmerchants.id', 'n_merchant')
            ->whereNotIn('n_merchant', ['Transfer'])
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->where('tmmerchant_tmpemilik.tmpemilik_id', Auth::user()->tmpemilik_id)
            ->withCount('tmtransaksi')
            ->get();
        $check = TmMerchant_TmPemilik::where('tmpemilik_id', Auth::user()->tmpemilik_id)->first();

        /**
         * Count transaksi
         */
        $transaksi = TmTransaksi::join('tmmerchants', 'tmtransaksis.tmmerchant_id', '=', 'tmmerchants.id')
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->where('tmmerchant_tmpemilik.tmpemilik_id', Auth::user()->tmpemilik_id)
            ->count();
        $user_transaksi = DB::select('
            SELECT
                COUNT( order_id ) AS total_transaksi,
                username,
                full_name 
            FROM
                tmtransaksis
                JOIN tmmerchants ON tmtransaksis.tmmerchant_id = tmmerchants.id
                JOIN tmmerchant_tmpemilik ON tmmerchants.id = tmmerchant_tmpemilik.tmmerchant_id
                JOIN users ON tmtransaksis.user_sender_id = users.id
                JOIN user_details ON users.id = user_details.user_id 
            WHERE
                tmmerchant_tmpemilik.tmpemilik_id = "' . Auth::user()->tmpemilik_id . '"
            GROUP BY
                username,full_name
            ORDER BY
                total_transaksi;
        ');

        /**
         * Count User by Pemilik
         */
        $userPemilik = UserTangselPay::select(DB::raw('COUNT(users.username) as total_user'), 'tmpemiliks.nama')
            ->join('tmpemiliks', 'users.tmpemilik_id', '=', 'tmpemiliks.id')
            ->groupBy('nama')
            ->get();

        /**
         * Merchant by pemilik
         */
        $merchantPemilik = TmMerchant_TmPemilik::select('tmmerchants.n_merchant', 'tmmerchants.icon')
            ->join('tmmerchants', 'tmmerchant_tmpemilik.tmmerchant_id', '=', 'tmmerchants.id')
            ->join('tmpemiliks', 'tmmerchant_tmpemilik.tmpemilik_id', '=', 'tmpemiliks.id')
            ->where('tmpemilik_id', Auth::user()->tmpemilik_id)
            ->get();

        /**
         * Metode Pembayaran
         */
        $metodePembayaran = Payment::select('id', 'n_payment', 'icon')->get();
        $pembayaran = SubPayment::select('id', 'n_payment_sub', 'icon')->get();

        /**
         * History
         */
        $history = TmTransaksi::select('user_details.full_name AS nama', 'tmmerchants.n_merchant AS merchant', 'tmtransaksis.total_bayar')
            ->join('tmmerchants', 'tmtransaksis.tmmerchant_id', '=', 'tmmerchants.id')
            ->join('users', 'tmtransaksis.user_sender_id', '=', 'users.id')
            ->join('user_details', 'users.id', '=', 'user_details.user_id')
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->where('tmmerchant_tmpemilik.tmpemilik_id', Auth::user()->tmpemilik_id)
            ->where('tmtransaksis.status', 1)
            ->take(15)
            ->latest('tmtransaksis.created_at')
            ->get();
        $total_transaksi = TmTransaksi::join('tmmerchants', 'tmtransaksis.tmmerchant_id', '=', 'tmmerchants.id')
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->where('tmmerchant_tmpemilik.tmpemilik_id', Auth::user()->tmpemilik_id)
            ->where('tmtransaksis.status', 1)
            ->sum('tmtransaksis.total_bayar');

        return view('pages.dashboard.index', compact(
            'tmmerchants',
            'check',
            'transaksi',
            'user_transaksi',
            'userPemilik',
            'merchantPemilik',
            'metodePembayaran',
            'pembayaran',
            'role',
            'history',
            'total_transaksi'
        ));
    }
}
