<?php

namespace App\Http\Controllers\MasterPemilik;

use Auth;
use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmPemilik;
use App\Models\PemilikKetentuan;

class PemilikKetentuanController extends Controller
{
    protected $route = 'master-pemilik.pemilikKetentuan.';
    protected $view  = 'pages.masterPemilik.pemilikKetentuan.';
    protected $title = 'Pemilik Ketentuan';
    protected $path  = 'assets/icon/profile/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik'
        ));
    }

    public function api()
    {
        $pemilikKetentuan = PemilikKetentuan::where('tmpemilik_id', Auth::user()->tmpemilik_id)->get();
        return DataTables::of($pemilikKetentuan)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('tmpemilik_id', function ($p) {
                return $p->tmpemilik->nama;
            })
            ->editColumn('ketentuan', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->ketentuan . "</a>";
            })
            ->editColumn('icon',  function ($p) {
                if ($p->icon != null) {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon . "'>";
                } else {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'icon', 'ketentuan'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilikKetentuan = PemilikKetentuan::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'pemilikKetentuan'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'ketentuan'    => 'required',
            'deskripsi'    => 'required',
            'status'       => 'required',
            'icon'         => 'required|mimes:png,jpg,jpeg|max:2024'
        ]);

        $tmpemilik_id = $request->tmpemilik_id;
        $ketentuan  = $request->ketentuan;
        $deskripsi  = $request->deskripsi;
        $status     = $request->status;

        $pemilikKetentuan = new PemilikKetentuan();
        $pemilikKetentuan->tmpemilik_id = $tmpemilik_id;
        $pemilikKetentuan->ketentuan    = $ketentuan;
        $pemilikKetentuan->status       = $status;

        $file     = $request->file('icon');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

        $pemilikKetentuan->deskripsi = $deskripsi;
        $pemilikKetentuan->icon = $fileName;
        $pemilikKetentuan->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $pemilikKetentuan = PemilikKetentuan::findOrFail($id);

        return $pemilikKetentuan;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'ketentuan'    => 'required',
            'status'       => 'required',
            'deskripsi'    => 'required'
        ]);

        $pemilikKetentuan = PemilikKetentuan::find($id);
        $ketentuan = $request->ketentuan;
        $deskripsi = $request->deskripsi;
        $status    = $request->status;
        $tmpemilik_id = $request->tmpemilik_id;

        if ($request->icon != null) {
            $request->validate([
                'icon' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $pemilikKetentuan->icon;
            Storage::disk('sftp')->delete($this->path . $exist);

            $pemilikKetentuan->update([
                'ketentuan' => $ketentuan,
                'deskripsi' => $deskripsi,
                'tmpemilik_id' => $tmpemilik_id,
                'status' => $status,
                'icon' => $fileName
            ]);
        } else {
            $pemilikKetentuan->update([
                'ketentuan' => $ketentuan,
                'deskripsi' => $deskripsi,
                'status' => $status,
                'tmpemilik_id' => $tmpemilik_id
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $pemilikKetentuan = PemilikKetentuan::findOrFail($id);
        $exist = $pemilikKetentuan->icon;

        // Delete icon from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmpemilik_ketentuans
        $pemilikKetentuan->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
