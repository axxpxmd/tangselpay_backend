<?php

namespace App\Http\Controllers\MasterPemilik;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmPemilik;

class PemilikController extends Controller
{
    protected $route = 'master-pemilik.pemilik.';
    protected $view  = 'pages.masterPemilik.pemilik.';
    protected $title = 'Pemilik';
    protected $path  = 'assets/icon/logo/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path'
        ));
    }

    public function api()
    {
        $app = TmPemilik::all();
        return DataTables::of($app)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('nama', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary' title='Show Data'>" . $p->nama . "</a>";
            })
            ->editColumn('logo',  function ($p) {
                if ($p->logo != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->logo . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'nama', 'logo'])
            ->toJson();
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'    => 'required',
            'logo'    =>  'required|mimes:png,jpg,jpeg|max:2024',
            'alamat'  => 'required',
            'no_tlpn' => 'required|unique:tmpemiliks',
            'logo_text'         => 'required',
            'server_key'        => 'required',
            'nm_perusahaan'     => 'required',
            'alamat_perusahaan' => 'required'
        ]);

        $nama   = $request->nama;
        $alamat = $request->alamat;
        $no_tlpn    = $request->no_tlpn;
        $logo_text  = $request->logo_text;
        $server_key = $request->server_key;
        $nm_perusahaan     = $request->nm_perusahaan;
        $alamat_perusahaan = $request->alamat_perusahaan;

        $tmpemilik = new TmPemilik();
        $tmpemilik->nama   = $nama;
        $tmpemilik->alamat = $alamat;
        $tmpemilik->no_tlpn    = $no_tlpn;
        $tmpemilik->logo_text  = $logo_text;
        $tmpemilik->server_key = $server_key;
        $tmpemilik->nm_perusahaan     = $nm_perusahaan;
        $tmpemilik->alamat_perusahaan = $alamat_perusahaan;

        $file     = $request->file('logo');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('logo')->storeAs($this->path, $fileName, 'sftp', 'public');

        $tmpemilik->logo = $fileName;
        $tmpemilik->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $tmpemilik = TmPemilik::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'tmpemilik',
            'path'
        ));
    }

    public function edit($id)
    {
        $tmpemilik = TmPemilik::findOrFail($id);

        return $tmpemilik;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'    => 'required',
            'alamat'  => 'required',
            'no_tlpn' => 'required|unique:tmpemiliks,no_tlpn,' . $id,
            'logo_text'         => 'required',
            'server_key'        => 'required',
            'nm_perusahaan'     => 'required',
            'alamat_perusahaan' => 'required'
        ]);

        $tmpemilik = TmPemilik::findOrFail($id);
        $nama   = $request->nama;
        $alamat = $request->alamat;
        $no_tlpn    = $request->no_tlpn;
        $logo_text  = $request->logo_text;
        $server_key = $request->server_key;
        $nm_perusahaan     = $request->nm_perusahaan;
        $alamat_perusahaan = $request->alamat_perusahaan;

        if ($request->logo != null) {
            $request->validate([
                'logo'    =>  'required|mimes:png,jpg,jpeg|max:2024',
            ]);

            $file     = $request->file('logo');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('logo')->storeAs($this->path, $fileName, 'sftp', 'public');

            $exist = $tmpemilik->logo;
            Storage::disk('sftp')->delete($this->path . $exist);

            $tmpemilik->update([
                'nama'       => $nama,
                'logo'       => $fileName,
                'alamat'     => $alamat,
                'no_tlpn'    => $no_tlpn,
                'logo_text'  => $logo_text,
                'server_key' => $server_key,
                'nm_perusahaan'     => $nm_perusahaan,
                'alamat_perusahaan' => $alamat_perusahaan
            ]);
        } else {
            $tmpemilik->update([
                'nama'       => $nama,
                'alamat'     => $alamat,
                'no_tlpn'    => $no_tlpn,
                'logo_text'  => $logo_text,
                'server_key' => $server_key,
                'nm_perusahaan'     => $nm_perusahaan,
                'alamat_perusahaan' => $alamat_perusahaan
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $tmpemilik  = TmPemilik::findOrFail($id);
        $exist      = $tmpemilik->logo;

        // delete logo from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmpemiliks
        $tmpemilik->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
