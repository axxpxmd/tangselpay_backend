<?php

namespace App\Http\Controllers\MasterMedia;

use Auth;
use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\Berita;
use App\Models\TmPemilik;

class BeritaController extends Controller
{
    protected $route = 'master-media.berita.';
    protected $view  = 'pages.masterMedia.berita.';
    protected $title = 'Berita';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'pemilik'
        ));
    }

    public function api()
    {
        $berita = Berita::where('tmpemilik_id', Auth::user()->tmpemilik_id)->get();
        return DataTables::of($berita)
            ->addColumn('action', function ($p) {
                return "
                <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;

        $berita = Berita::findOrFail($id);

        return view($this->view . 'show', compact(
            'route',
            'title',
            'berita'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_news' => 'required',
            'url'    => 'required'
        ]);

        $berita = new Berita();
        $berita->n_news       = $request->n_news;
        $berita->tmpemilik_id = $request->tmpemilik_id;
        $berita->url = $request->url;
        $berita->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $berita = Berita::find($id);

        return $berita;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_news' => 'required',
            'url'    => 'required'
        ]);

        $berita = Berita::findOrFail($id);
        $tmpemilik_id = $request->tmpemilik_id;
        $n_news = $request->n_news;
        $url    = $request->url;

        $berita->update([
            'tmpemilik_id' => $tmpemilik_id,
            'n_news' => $n_news,
            'url'    => $url
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        Berita::destroy($id);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
