<?php

namespace App\Http\Controllers\MasterMedia;

use Auth;
use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Models
use App\Models\Iklan;
use App\Models\TmPemilik;

class IklanController extends Controller
{
    protected $route = 'master-media.iklan.';
    protected $view  = 'pages.masterMedia.iklan.';
    protected $title = 'Iklan';
    protected $path  = 'assets/iklan/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik'
        ));
    }

    public function api()
    {
        $iklan = Iklan::where('tmpemilik_id', Auth::user()->tmpemilik_id)->get();
        return DataTables::of($iklan)
            ->addColumn('action', function ($p) {
                return "
                <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->editColumn('image',  function ($p) {
                if ($p->image != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->image . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'image'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $iklan = Iklan::findOrFail($id);

        return view($this->view . 'show', compact(
            'route',
            'title',
            'path',
            'iklan'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_iklan' => 'required',
            'ket'     => 'required',
            'image'   => 'required|mimes:png,jpg,jpeg|max:2024',
        ]);

        $tmpemilik_id = $request->tmpemilik_id;
        $n_iklan = $request->n_iklan;
        $ket     = $request->ket;

        $iklan = new Iklan();
        $iklan->tmpemilik_id = $tmpemilik_id;
        $iklan->n_iklan = $n_iklan;
        $iklan->ket     = $ket;

        $file     = $request->file('image');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('image')->storeAs($this->path, $fileName, 'sftp', 'public');

        $iklan->image = $fileName;
        $iklan->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $iklan = Iklan::find($id);

        return $iklan;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_iklan' => 'required',
            'ket'     => 'required'
        ]);

        $iklan = Iklan::find($id);
        $n_iklan      = $request->n_iklan;
        $ket          = $request->ket;
        $tmpemilik_id = $request->tmpemilik_id;

        if ($request->image != null) {
            $request->validate([
                'image'   => 'required|mimes:png,jpg,jpeg|max:2024',
            ]);

            $file     = $request->file('image');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('image')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $iklan->image;
            Storage::disk('sftp')->delete($this->path . $exist);

            $iklan->update([
                'tmpemilik_id' => $tmpemilik_id,
                'n_iklan' => $n_iklan,
                'ket'     => $ket,
                'image'   => $fileName
            ]);
        } else {
            $iklan->update([
                'tmpemilik_id' => $tmpemilik_id,
                'n_iklan' => $n_iklan,
                'ket'     => $ket,
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $iklan = Iklan::findOrFail($id);
        $exist = $iklan->image;

        // Delete image from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmiklans
        $iklan->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
