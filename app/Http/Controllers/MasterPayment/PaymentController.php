<?php

namespace App\Http\Controllers\MasterPayment;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\Payment;

class PaymentController extends Controller
{
    protected $route = 'master-payment.payment.';
    protected $view  = 'pages.masterPayments.payment.';
    protected $title = 'Payment';
    protected $path  = 'assets/icon/payment/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path'
        ));
    }

    public function api()
    {
        $payment = Payment::all();
        return DataTables::of($payment)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('n_payment', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->n_payment . "</a>";
            })
            ->editColumn('icon',  function ($p) {
                if ($p->icon != null) {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon . "'>";
                } else {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'n_payment', 'icon'])
            ->toJson();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;

        $payment = Payment::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'payment'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'n_payment' => 'required|unique:tmpayments',
            'route' => 'required',
            'ket'   => 'required',
            'icon'  => 'required|mimes:png,jpg,jpeg|max:2024'
        ]);

        $payment = new Payment();
        $payment->n_payment = $request->n_payment;
        $payment->route = '/' . $request->route;
        $payment->ket   = $request->ket;

        $file     = $request->file('icon');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

        $payment->icon = $fileName;
        $payment->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $payment = Payment::findOrFail($id);

        return $payment;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'n_payment' => 'required|unique:tmpayments,n_payment,' . $id,
            'route' => 'required',
            'ket'   => 'required'
        ]);

        $payment   = Payment::find($id);
        $n_payment = $request->n_payment;
        $route = $request->route;
        $ket   = $request->ket;

        if ($request->icon != null) {
            $request->validate([
                'icon' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $payment->icon;
            Storage::disk('sftp')->delete($this->path . $exist);

            $payment->update([
                'n_payment' => $n_payment,
                'route' => $route,
                'ket'   => $ket,
                'icon'  => $fileName
            ]);
        } else {
            $payment->update([
                'n_payment' => $n_payment,
                'route' => $route,
                'ket'   => $ket
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $payment = Payment::findOrFail($id);
        $exist   = $payment->icon;

        // Delete icon from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmpayments
        $payment->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
