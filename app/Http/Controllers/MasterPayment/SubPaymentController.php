<?php

namespace App\Http\Controllers\MasterPayment;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\Payment;
use App\Models\SubPayment;

class SubPaymentController extends Controller
{
    protected $route = 'master-payment.subPayment.';
    protected $view  = 'pages.masterPayments.subPayment.';
    protected $title = 'Sub Payment';
    protected $path  = 'assets/icon/payment/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $payment = Payment::select('id', 'n_payment')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'payment'
        ));
    }

    public function api(Request $request)
    {
        $subPayment = SubPayment::all();

        if ($request->payment_id) {
            $subPayment = SubPayment::where('tmpayment_id', $request->payment_id)->get();
        }
        return DataTables::of($subPayment)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('tmpayment_id', function ($p) {
                return $p->tmpayment->n_payment;
            })
            ->editColumn('n_payment_sub', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->n_payment_sub . "</a>";
            })
            ->editColumn('icon',  function ($p) {
                if ($p->icon != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'n_payment_sub', 'icon', 'tmpayment_id'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $subPayment = SubPayment::findOrFail($id);

        return view($this->view . 'show', compact(
            'route',
            'title',
            'path',
            'subPayment'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpayment_id'  => 'required',
            'n_payment_sub' => 'required',
            'icon'  => 'required|mimes:png,jpg,jpeg|max:2024',
            'ket'   => 'required'
        ]);

        $subPayment = new SubPayment();
        $subPayment->tmpayment_id  = $request->tmpayment_id;
        $subPayment->n_payment_sub = $request->n_payment_sub;
        $subPayment->ket = $request->ket;

        $file     = $request->file('icon');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

        $subPayment->icon = $fileName;
        $subPayment->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $subPayment = SubPayment::find($id);

        return $subPayment;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpayment_id'  => 'required',
            'n_payment_sub' => 'required',
            'ket'   => 'required'
        ]);

        $subPayment    = SubPayment::find($id);
        $n_payment_sub = $request->n_payment_sub;
        $tmpayment_id  = $request->tmpayment_id;
        $ket = $request->ket;

        if ($request->icon != null) {
            $request->validate([
                'icon'  => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $subPayment->icon;
            Storage::disk('sftp')->delete($this->path . $exist);

            $subPayment->update([
                'n_payment_sub' => $n_payment_sub,
                'tmpayment_id'  => $tmpayment_id,
                'ket'  => $ket,
                'icon' => $fileName
            ]);
        } else {
            $subPayment->update([
                'n_payment_sub' => $n_payment_sub,
                'tmpayment_id'  => $tmpayment_id,
                'ket'  => $ket,
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $subPayment = SubPayment::findOrFail($id);
        $exist      = $subPayment->icon;

        // Delete icon from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmpayment_subs
        $subPayment->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
