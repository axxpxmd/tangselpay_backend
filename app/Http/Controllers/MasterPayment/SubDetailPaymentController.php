<?php

namespace App\Http\Controllers\MasterPayment;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\Payment;
use App\Models\SubDetailPayment;
use App\Models\SubPayment;

class SubDetailPaymentController extends Controller
{
    protected $route = 'master-payment.subDetailPayment.';
    protected $view  = 'pages.masterPayments.subDetailPayment.';
    protected $title = 'Sub Detail Payment';
    protected $path  = 'assets/icon/payment/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $payment    = Payment::select('id', 'n_payment')->get();
        $subPayment = SubPayment::select('id', 'n_payment_sub')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'payment',
            'subPayment'
        ));
    }

    public function api(Request $request)
    {
        $subDetailPayment = SubDetailPayment::all();

        $payment_id     = $request->payment_id;
        $sub_payment_id = $request->sub_payment_id;

        if ($sub_payment_id != null) {
            $subDetailPayment = SubDetailPayment::select('tmpayment_sub_details.id', 'tmpayment_sub_details.n_payment_sub_detail', 'tmpayment_sub_details.ket', 'tmpayment_sub_id', 'no_rek')
                ->join('tmpayment_subs', 'tmpayment_sub_details.tmpayment_sub_id', '=', 'tmpayment_subs.id')
                ->where('tmpayment_subs.id', $sub_payment_id)->get();
        } elseif ($payment_id != null) {
            $subDetailPayment = SubDetailPayment::select('tmpayment_sub_details.id', 'tmpayment_sub_details.n_payment_sub_detail', 'tmpayment_sub_details.ket', 'tmpayment_sub_id', 'no_rek')
                ->join('tmpayment_subs', 'tmpayment_sub_details.tmpayment_sub_id', '=', 'tmpayment_subs.id')
                ->join('tmpayments', 'tmpayment_subs.tmpayment_id', '=', 'tmpayments.id')
                ->where('tmpayments.id', $payment_id)->get();
        }

        return DataTables::of($subDetailPayment)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('n_payment_sub_detail', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->n_payment_sub_detail . "</a>";
            })
            ->editColumn('tmpayment_sub_id', function ($p) {
                return $p->sub_payment->n_payment_sub;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'n_payment_sub_detail', 'tmpayment_sub_id'])
            ->toJson();
    }

    public function subPaymentByPayment($payment_id)
    {
        return SubPayment::select('id', 'n_payment_sub')->where('tmpayment_id', $payment_id)->get();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;

        $subDetailPayment = SubDetailPayment::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'subDetailPayment'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'ket'    => 'required',
            'no_rek' => 'required',
            'n_payment_sub_detail' => 'required',
            'tmpayment_sub_id'     => 'required'
        ]);

        $subDetail = new SubDetailPayment();
        $subDetail->n_payment_sub_detail = $request->n_payment_sub_detail;
        $subDetail->tmpayment_sub_id     = $request->tmpayment_sub_id;
        $subDetail->ket    = $request->ket;
        $subDetail->no_rek = $request->no_rek;
        $subDetail->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $subDetail = SubDetailPayment::find($id);

        return $subDetail;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'ket'    => 'required',
            'no_rek' => 'required',
            'n_payment_sub_detail' => 'required',
            'tmpayment_sub_id'     => 'required'
        ]);

        $subDetail = SubDetailPayment::find($id);
        $subDetail->update([
            'ket'    => $request->ket,
            'no_rek' => $request->no_rek,
            'n_payment_sub_detail' => $request->n_payment_sub_detail,
            'tmpayment_sub_id'     => $request->tmpayment_sub_id
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        SubDetailPayment::destroy($id);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
