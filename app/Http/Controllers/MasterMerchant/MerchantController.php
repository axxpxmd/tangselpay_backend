<?php

namespace App\Http\Controllers\MasterMerchant;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmPemilik;
use App\Models\TmMerchant;
use App\Models\TmJenis_Merchants;
use App\Models\TmMerchant_TmPemilik;

class MerchantController extends Controller
{
    protected $route = 'master-merchant.merchant.';
    protected $view  = 'pages.masterMerchants.merchant.';
    protected $title = 'Merchant';
    protected $path  = 'assets/icon/merchant/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik       = TmPemilik::select('id', 'nama')->get();
        $jenisMerchant = TmJenis_Merchants::select('id', 'n_jenis_merchant')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik',
            'jenisMerchant'
        ));
    }

    public function api(Request $request)
    {
        $pemilik_id        = $request->pemilik_id;
        $jenis_merchant_id = $request->jenis_merchant_id;

        $tmmerchant = TmMerchant::select(
            'tmmerchants.id',
            'tmmerchants.n_merchant',
            'tmmerchants.icon',
            'tmmerchants.ket',
            'tmmerchants.tmjenis_merchant_id',
            'tmmerchant_tmpemilik.tmpemilik_id as tmerchant_pemilik'
        )
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->with('tmtransaksi')->get();

        /**
         * Filter By Jenis Merchant
         */
        if ($jenis_merchant_id != null) {
            $tmmerchant = TmMerchant::select(
                'tmmerchants.id',
                'tmmerchants.n_merchant',
                'tmmerchants.icon',
                'tmmerchants.ket',
                'tmmerchants.tmjenis_merchant_id',
                'tmmerchant_tmpemilik.tmpemilik_id as tmerchant_pemilik'
            )
                ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
                ->where('tmmerchants.tmjenis_merchant_id', $jenis_merchant_id)
                ->with('tmtransaksi')->get();
        }

        /**
         * Filter By Pemilik
         */
        if ($pemilik_id != null) {
            $tmmerchant = TmMerchant::select(
                'tmmerchants.id',
                'tmmerchants.n_merchant',
                'tmmerchants.icon',
                'tmmerchants.ket',
                'tmmerchants.tmjenis_merchant_id',
                'tmmerchant_tmpemilik.tmpemilik_id as tmerchant_pemilik'
            )
                ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
                ->where('tmmerchant_tmpemilik.tmpemilik_id', $pemilik_id)
                ->with('tmtransaksi')->get();
            // Filter By Pemilik and Jenis Merchant
            if ($jenis_merchant_id != null) {
                $tmmerchant = TmMerchant::select(
                    'tmmerchants.id',
                    'tmmerchants.n_merchant',
                    'tmmerchants.icon',
                    'tmmerchants.ket',
                    'tmmerchants.tmjenis_merchant_id',
                    'tmmerchant_tmpemilik.tmpemilik_id as tmerchant_pemilik'
                )
                    ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
                    ->where('tmmerchant_tmpemilik.tmpemilik_id', $pemilik_id)
                    ->where('tmmerchants.tmjenis_merchant_id', $jenis_merchant_id)
                    ->with('tmtransaksi')->get();
            }
        }
        return DataTables::of($tmmerchant)
            ->addColumn('action', function ($p) {
                return "
                    <a href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('n_merchant', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->n_merchant . "</a>";
            })
            ->addColumn('nama_pemilik', function ($p) {
                $pemilik = TmPemilik::whereid($p->tmerchant_pemilik)->first();
                return $pemilik->nama;
            })
            ->editColumn('tmjenis_merchant_id', function ($p) {
                if ($p->tmjenis_merchant != null) {
                    return $p->tmjenis_merchant->n_jenis_merchant;
                } else {
                    return '-';
                }
            })
            ->editColumn('icon',  function ($p) {
                if ($p->icon != null) {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon . "'>";
                } else {
                    return "<img width='30' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmjenis_merchant_id', 'n_merchant', 'icon', 'nama_pemilik'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $merchant = TmMerchant::findOrFail($id);
        $pemilik  = TmMerchant_TmPemilik::where('tmmerchant_id', $merchant->id)->first();

        return view($this->view . 'show', compact(
            'route',
            'title',
            'path',
            'merchant',
            'pemilik'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_merchant'   => 'required',
            'position'     => 'required',
            'ket'    => 'required',
            'icon'   => 'required|mimes:png,jpg,jpeg|max:2024',
            'status' => 'required',
            'route'  => 'required',
            'kode'   => 'required',
            'tmjenis_merchant_id' => 'required'
        ]);

        /** Tahapan :
         * 1. tmmerchants
         * 2. tmmerchant_tmpemilik
         */

        // Tahap 1
        $tmjenis_merchant_id = $request->tmjenis_merchant_id;
        $n_merchant = $request->n_merchant;
        $status     = $request->status;
        $route      = $request->route;
        $kode       = $request->kode;
        $ket        = $request->ket;

        $tmmerchants = new TmMerchant();
        $tmmerchants->tmjenis_merchant_id = $tmjenis_merchant_id;
        $tmmerchants->n_merchant = $n_merchant;
        $tmmerchants->status     = $status;
        $tmmerchants->route      = '/' . $route;
        $tmmerchants->kode       = $kode;
        $tmmerchants->ket        = $ket;

        $file     = $request->file('icon');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

        $tmmerchants->icon = $fileName;
        $tmmerchants->save();

        // Tahap 2
        $get_merchant_id = TmMerchant::select('id')->latest()->first();
        $tmpemilik_id = $request->tmpemilik_id;
        $position     = $request->position;

        $tmerchant_pemilik = new TmMerchant_TmPemilik();
        $tmerchant_pemilik->tmpemilik_id  = $tmpemilik_id;
        $tmerchant_pemilik->tmmerchant_id = $get_merchant_id->id;
        $tmerchant_pemilik->position      = $position;
        $tmerchant_pemilik->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $tmmerchant = TmMerchant::select(
            'tmmerchants.id',
            'tmmerchants.n_merchant',
            'tmmerchants.icon',
            'tmmerchants.ket',
            'tmmerchants.status',
            'tmmerchants.route',
            'tmmerchants.kode',
            'tmmerchants.tmjenis_merchant_id',
            'tmmerchant_tmpemilik.position',
            'tmmerchant_tmpemilik.tmpemilik_id'
        )
            ->join('tmmerchant_tmpemilik', 'tmmerchants.id', '=', 'tmmerchant_tmpemilik.tmmerchant_id')
            ->where('tmmerchants.id', $id)
            ->with('tmtransaksi')->first();

        return $tmmerchant;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'n_merchant'   => 'required',
            'position'     => 'required',
            'ket'    => 'required',
            'status' => 'required',
            'route'  => 'required',
            'kode'   => 'required',
            'tmjenis_merchant_id' => 'required'
        ]);

        /** Tahapan :
         * 1. tmmerchants
         * 2. tmmerchant_tmpemilik
         */

        // Tahap 1
        $merchant = TmMerchant::findOrFail($id);
        $tmjenis_merchant_id = $request->tmjenis_merchant_id;
        $n_merchant = $request->n_merchant;
        $status     = $request->status;
        $route      = $request->route;
        $kode       = $request->kode;
        $ket        = $request->ket;

        if ($request->icon != null) {
            $request->validate([
                'icon'   => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $merchant->icon;
            Storage::disk('sftp')->delete($this->path . $exist);

            $merchant->update([
                'n_merchant' => $n_merchant,
                'status'     => $status,
                'route'      => $route,
                'kode'       => $kode,
                'ket'        => $ket,
                'icon'       => $fileName,
                'tmjenis_merchant_id' => $tmjenis_merchant_id
            ]);
        } else {
            $merchant->update([
                'n_merchant' => $n_merchant,
                'status'     => $status,
                'route'      => $route,
                'kode'       => $kode,
                'ket'        => $ket,
                'tmjenis_merchant_id' => $tmjenis_merchant_id
            ]);
        }

        // Tahap 2
        $tmerchant_pemilik = TmMerchant_TmPemilik::where('tmmerchant_id', $merchant->id)->first();
        $tmpemilik_id = $request->tmpemilik_id;
        $position     = $request->position;

        $tmerchant_pemilik->update([
            'tmpemilik_id' => $tmpemilik_id,
            'position'     => $position
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $merchant   = TmMerchant::findOrFail($id);
        $exist = $merchant->icon;

        // Delete icon from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmerchant_pemilik
        TmMerchant_TmPemilik::where('tmmerchant_id', $merchant->id)->delete();

        // delete from tabel tmmerchants
        $merchant->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
