<?php

namespace App\Http\Controllers\MasterMerchant;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\TmJenis_Merchants;

class JenisMerchantController extends Controller
{
    protected $route = 'master-merchant.jenisMerchant.';
    protected $view  = 'pages.masterMerchants.jenisMerchant.';
    protected $title = 'Jenis Merchant';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        return view($this->view . 'index', compact(
            'route',
            'title'
        ));
    }

    public function api()
    {
        $jenisMerchant = TmJenis_Merchants::all();
        return Datatables::of($jenisMerchant)
            ->addColumn('action', function ($p) {
                return "
                    <a href='#' onclick='edit(" . $p->id . ")' title='Edit Role'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Role'><i class='icon-remove'></i></a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->toJson();
    }

    public function store(Request $request)
    {
        $request->validate([
            'n_jenis_merchant' => 'required|unique:tmjenis_merchants'
        ]);

        $jenisMerchant = new TmJenis_Merchants();
        $jenisMerchant->n_jenis_merchant = $request->n_jenis_merchant;
        $jenisMerchant->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $jenisMerchant = TmJenis_Merchants::findOrFail($id);

        return $jenisMerchant;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'n_jenis_merchant' => 'required|unique:tmjenis_merchants,n_jenis_merchant,' . $id
        ]);

        $jenisMerchant = TmJenis_Merchants::find($id);
        $jenisMerchant->update([
            'n_jenis_merchant' => $request->n_jenis_merchant
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        TmJenis_Merchants::destroy($id);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
