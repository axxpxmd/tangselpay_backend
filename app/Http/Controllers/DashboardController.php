<?php

namespace App\Http\Controllers;

use App\Models\TmPemilik;
use Auth;

use App\Models\TmTransaksi;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

// Model
use App\Models\UserTangselPay;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Time
        $time  = Carbon::now();
        $tahun_filter = $request->tahun ? $request->tahun : $time->format('Y');
        $year  = $tahun_filter ? $tahun_filter : $time->format('Y');

        /*
         * Drilldown Chart
         */
        $userYears = UserTangselPay::select(DB::raw("(COUNT(*)) as total"), DB::raw("YEAR(created_at) as tahun"))
            ->groupBy('tahun')
            ->orderBy('tahun', 'ASC')
            ->get();

        foreach ($userYears as $key => $i) {
            $color = ['#26a69a', '#26c6da', '#42a5f5', '#ef5350', '#ff7043', '#5c6bc0', '#ffee58', '#bdbdbd', '#66bb6a ', '#ec407a'];

            $parents[$key] = [
                'y'    => $i->total,
                'name' => $i->tahun,
                'color' => $color[$key],
                'drilldown' => $i->tahun,

            ];

            $userMonth = UserTangselPay::select(DB::raw("(COUNT(*)) as total"), DB::raw("MONTH(created_at) as bulan"))
                ->whereYear('created_at', $i->tahun)
                ->groupBy('bulan')
                ->orderBy('bulan', 'ASC')
                ->get();

            foreach ($userMonth as $key1 => $k) {
                $dataChills[$key1] = [
                    'name' => Carbon::create()->month($k->bulan)->format('F'),
                    'y'    => $k->total
                ];

                $childs[$key] = [
                    'name'  => $i->tahun,
                    'id'    => $i->tahun,
                    'data'  => $dataChills,
                    'color' => $color[$key]
                ];
            }
        }
        $parentJson = json_encode($parents);
        $childJson  = json_encode($childs);

        /*
         * Pie Chart
         */
        $totalLaki = UserTangselPay::join('user_details', 'user_details.user_id', '=', 'users.id')->where('user_details.jenis_kelamin', 'W')->whereYear('users.created_at', $tahun_filter)->count();
        $totalPerempuan = UserTangselPay::join('user_details', 'user_details.user_id', '=', 'users.id')->where('user_details.jenis_kelamin', 'L')->whereYear('users.created_at', $tahun_filter)->count();

        /*
         * Merchant 
         */
        $transaksiMerchant = TmTransaksi::select(DB::raw("SUM(total_bayar) as total_bayar"), DB::raw("COUNT(tmmerchant_id) as total_transaksi"), 'tmmerchant_id')
            ->groupBy('tmmerchant_id')
            ->orderBy('total_transaksi', 'DESC')
            ->whereYear('created_at', $tahun_filter)
            ->get();

        /*
         * Pemilik
         */
        $pemiliks = TmPemilik::select('id', 'nama', 'logo')->get();

        /*
         * Total Transaksi 
         */
        $totalTransaksi = TmTransaksi::whereYear('tgl_transaksi', $tahun_filter)->sum('total_bayar');
        $listTotalTransaksi = TmTransaksi::select(DB::raw("SUM(total_bayar) as total_bayar"), DB::raw("YEAR(tgl_transaksi) as tahun"))->groupBy('tahun')->get();

        return view('pages.dashboard2.index', compact(
            'parentJson',
            'childJson',
            'totalLaki',
            'totalPerempuan',
            'transaksiMerchant',
            'pemiliks',
            'year',
            'totalTransaksi',
            'listTotalTransaksi'
        ));
    }
}
