<?php

namespace App\Http\Controllers\MasterConfig;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmConfig;
use App\Models\TmPemilik;

class ThemeController extends Controller
{
    protected $route = 'master-config.theme.';
    protected $view  = 'pages.masterConfig.theme.';
    protected $title = 'Config Theme';
    protected $path  = 'assets/icon/logo/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'pemilik'
        ));
    }

    public function api()
    {
        $theme = TmConfig::all();
        return DataTables::of($theme)
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route('master-config.theme.show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->editColumn('primary_color', function ($p) {
                if ($p->primary_color == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->primary_color</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->primary_color . "'></span>";
                }
            })
            ->editColumn('button_color', function ($p) {
                if ($p->button_color  == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return  "<span class='fs-12'>$p->button_color</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->button_color . "'></span>";
                }
            })
            ->editColumn('background_color', function ($p) {
                if ($p->background_color == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->background_color</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->background_color . "'></span>";
                }
            })
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'primary_color', 'button_color', 'background_color'])
            ->toJson();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;

        $tmconfig = TmConfig::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'tmconfig'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'primary_color' => 'required|max:7',
            'button_color'  => 'required|max:7',
            'background_color' => 'required|max:7',
            'tmpemilik_id' => 'required'
        ]);

        $check = TmConfig::where('tmpemilik_id', $request->tmpemilik_id)->first();
        if ($check != null) {
            $tmpemilik = TmPemilik::where('id', $request->tmpemilik_id)->first();
            return response()->json([
                'message' => 'Pemilik ' . $tmpemilik->nama . ' sudah ada, silahkan di edit.'
            ], 422);
        } else {
            $primary_color = \strtoupper(substr($request->primary_color, 1));
            $button_color  = \strtoupper(substr($request->button_color, 1));
            $background_color = \strtoupper(substr($request->background_color, 1));

            $config_theme = new TmConfig();
            $config_theme->primary_color = $primary_color;
            $config_theme->button_color  = $button_color;
            $config_theme->background_color = $background_color;
            $config_theme->tmpemilik_id     = $request->tmpemilik_id;
            $config_theme->save();
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $tmconfig = TmConfig::findOrFail($id);
        return $tmconfig;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id'  => 'required|unique:tmconfigs,tmpemilik_id,' . $id,
            'primary_color' => 'required|max:7',
            'button_color'  => 'required|max:7',
            'background_color' => 'required|max:7'
        ]);

        if (\strlen($request->primary_color) == 7) {
            $primary_color = \strtoupper(substr($request->primary_color, 1));
        } else {
            $primary_color = \strtoupper($request->primary_color);
        }

        if (\strlen($request->button_color) == 7) {
            $button_color = \strtoupper(substr($request->button_color, 1));
        } else {
            $button_color = \strtoupper($request->button_color);
        }

        if (\strlen($request->background_color) == 7) {
            $background_color = \strtoupper(substr($request->background_color, 1));
        } else {
            $background_color = \strtoupper($request->background_color);
        }

        $config_theme = TmConfig::findOrFail($id);
        $config_theme->update([
            'primary_color' => $primary_color,
            'button_color'  => $button_color,
            'background_color' => $background_color,
            'tmpemilik_id'     => $request->tmpemilik_id
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        TmConfig::destroy($id);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
