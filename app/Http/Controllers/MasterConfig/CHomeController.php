<?php

namespace App\Http\Controllers\MasterConfig;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use App\Models\TmConfig;
use App\Models\TmPemilik;

class CHomeController extends Controller
{
    protected $route = 'master-config.home.';
    protected $view  = 'pages.masterConfig.home.';
    protected $title = 'Config Home';
    protected $path  = 'assets/icon/logo/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'pemilik'
        ));
    }

    public function api()
    {
        $home = TmConfig::all();
        return DataTables::of($home)
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->editColumn('color', function ($p) {
                if ($p->color == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->color</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->color . "'></span>";
                }
            })
            ->editColumn('gradient',  function ($p) {
                if ($p->gradient == 1) {
                    return 'Ya';
                } else {
                    return 'Tidak';
                }
            })
            ->editColumn('gradient_1', function ($p) {
                if ($p->gradient_1  == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return  "<span class='fs-12'>$p->gradient_1</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->gradient_1 . "'></span>";
                }
            })
            ->editColumn('gradient_2', function ($p) {
                if ($p->gradient_2 == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->gradient_2</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->gradient_2 . "'></span>";
                }
            })
            ->editColumn('text_color', function ($p) {
                if ($p->text_color == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->text_color</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->text_color . "'></span>";
                }
            })
            ->editColumn('text_gradient', function ($p) {
                if ($p->text_gradient == 1) {
                    return 'Ya';
                } else {
                    return 'Tidak';
                }
            })
            ->editColumn('text_gradient_1', function ($p) {
                if ($p->text_gradient_1 == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->text_gradient_1</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->text_gradient_1 . "'></span>";
                }
            })
            ->editColumn('text_gradient_2', function ($p) {
                if ($p->text_gradient_2 == null) {
                    return "<span class='avatar avatar-sm circle' style='background-color: transparent'></span>";
                } else {
                    return "<span class='fs-12'>$p->text_gradient_2</span>" . "<br>" . "<span class='avatar avatar-sm circle' style='background-color:#" . $p->text_gradient_2 . "'></span>";
                }
            })
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'color', 'gradient', 'gradient_1', 'gradient_2', 'text_color', 'text_gradient_1', 'text_gradient_2', 'tmpemilik_id'])
            ->toJson();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;

        $tmconfig = TmConfig::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'tmconfig'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'tmpemilik_id' => 'required',
            'color'    => 'required|max:7',
            'gradient' => 'required',
            'gradient_1' => 'required|max:7',
            'gradient_2' => 'required|max:7',
            'text_color' => 'required|max:7',
            'text_gradient'   => 'required',
            'text_gradient_1' => 'required|max:7',
            'text_gradient_2' => 'required|max:7'
        ]);

        $check = TmConfig::where('tmpemilik_id', $request->tmpemilik_id)->first();
        if ($check != null) {
            $tmpemilik = TmPemilik::where('id', $request->tmpemilik_id)->first();
            return response()->json([
                'message' => 'Pemilik ' . $tmpemilik->nama . ' sudah ada, silahkan di edit.'
            ], 422);
        } else {
            $color = \strtoupper(substr($request->color, 1));
            $gradient_1 = \strtoupper(substr($request->gradient_1, 1));
            $gradient_2 = \strtoupper(substr($request->gradient_2, 1));
            $text_color = \strtoupper(substr($request->text_color, 1));
            $text_gradient_1 = \strtoupper(substr($request->text_gradient_1, 1));
            $text_gradient_2 = \strtoupper(substr($request->text_gradient_2, 1));

            $config_home = new TmConfig();
            $config_home->color    = $color;
            $config_home->gradient = $request->gradient;
            $config_home->gradient_1 = $gradient_1;
            $config_home->gradient_2 = $gradient_2;
            $config_home->text_color = $text_color;
            $config_home->text_gradient   = $request->text_gradient;
            $config_home->text_gradient_1 = $text_gradient_1;
            $config_home->text_gradient_2 = $text_gradient_2;
            $config_home->tmpemilik_id = 1;
            $config_home->save();
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $tmconfig = TmConfig::findOrFail($id);
        return $tmconfig;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required|unique:tmconfigs,tmpemilik_id,' . $id,
            'color'    => 'required|max:7',
            'gradient' => 'required',
            'gradient_1' => 'required|max:7',
            'gradient_2' => 'required|max:7',
            'text_color' => 'required|max:7',
            'text_gradient'   => 'required',
            'text_gradient_1' => 'required|max:7',
            'text_gradient_2' => 'required|max:7'
        ]);

        if (\strlen($request->color) == 7) {
            $color = \strtoupper(substr($request->color, 1));
        } else {
            $color = \strtoupper($request->color);
        }

        if (\strlen($request->gradient_1) == 7) {
            $gradient_1 = \strtoupper(substr($request->gradient_1, 1));
        } else {
            $gradient_1 = \strtoupper($request->gradient_1);
        }

        if (\strlen($request->gradient_2) == 7) {
            $gradient_2 = \strtoupper(substr($request->gradient_2, 1));
        } else {
            $gradient_2 = \strtoupper($request->gradient_2);
        }

        if (\strlen($request->text_color) == 7) {
            $text_color = \strtoupper(substr($request->text_color, 1));
        } else {
            $text_color = \strtoupper($request->text_color);
        }

        if (\strlen($request->text_gradient_1) == 7) {
            $text_gradient_1 = \strtoupper(substr($request->text_gradient_1, 1));
        } else {
            $text_gradient_1 = \strtoupper($request->text_gradient_1);
        }

        if (\strlen($request->text_gradient_2) == 7) {
            $text_gradient_2 = \strtoupper(substr($request->text_gradient_2, 1));
        } else {
            $text_gradient_2 = \strtoupper($request->text_gradient_2);
        }

        $config_home = TmConfig::findOrFail($id);
        $config_home->update([
            'color' => $color,
            'gradient' => $request->gradient,
            'gradient_1' => $gradient_1,
            'gradient_2' => $gradient_2,
            'text_color' => $text_color,
            'text_gradient'   => $request->text_gradient,
            'text_gradient_1' => $text_gradient_1,
            'text_gradient_2' => $text_gradient_2,
            'tmpemilik_id' => $request->tmpemilik_id
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        TmConfig::destroy($id);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
