<?php

namespace App\Http\Controllers\MasterConfig;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmConfig;
use App\Models\TmPemilik;

class AuthController extends Controller
{
    protected $route = 'master-config.auth.';
    protected $view  = 'pages.masterConfig.auth.';
    protected $title = 'Config Auth';
    protected $path  = 'assets/icon/auth/';
    protected $path2 = 'assets/icon/logo/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik'
        ));
    }

    public function api()
    {
        $auth = TmConfig::all();
        return DataTables::of($auth)
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('icon_otp',  function ($p) {
                if ($p->icon_otp != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon_otp . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->editColumn('icon_verify_otp',  function ($p) {
                if ($p->icon_verify_otp != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon_verify_otp . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->editColumn('icon_register',  function ($p) {
                if ($p->icon_register != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon_register . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->editColumn('icon_password',  function ($p) {
                if ($p->icon_password != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon_password . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'icon_otp', 'icon_verify_otp', 'icon_register', 'icon_password'])
            ->toJson();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;
        $path2 = $this->path2;

        $tmconfig = TmConfig::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'path2',
            'tmconfig'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'icon_otp' => 'required|mimes:png,jpg,jpeg|max:2024',
            'icon_verify_otp' => 'required|mimes:png,jpg,jpeg|max:2024',
            'icon_register'   => 'required|mimes:png,jpg,jpeg|max:2024',
            'icon_password'   => 'required|mimes:png,jpg,jpeg|max:2024',
            'tmpemilik_id'    => 'required'
        ]);

        $check = TmConfig::where('tmpemilik_id', $request->tmpemilik_id)->first();
        if ($check != null) {
            $tmpemilik = TmPemilik::where('id', $request->tmpemilik_id)->first();
            return response()->json([
                'message' => 'Pemilik ' . $tmpemilik->nama . ' sudah ada, silahkan di edit.'
            ], 422);
        } else {
            $file     = $request->file('icon_otp');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon_otp')->storeAs($this->path, $fileName, 'sftp', 'public');

            $file1     = $request->file('icon_verify_otp');
            $fileName1 = time() . "." . $file1->getClientOriginalName();
            $request->file('icon_verify_otp')->storeAs($this->path, $fileName1, 'sftp', 'public');

            $file2     = $request->file('icon_register');
            $fileName2 = time() . "." . $file2->getClientOriginalName();
            $request->file('icon_register')->storeAs($this->path, $fileName2, 'sftp', 'public');

            $file3     = $request->file('icon_password');
            $fileName3 = time() . "." . $file3->getClientOriginalName();
            $request->file('icon_password')->storeAs($this->path, $fileName3, 'sftp', 'public');

            $config_auth = new TmConfig();
            $config_auth->icon_otp = $fileName;
            $config_auth->icon_verify_otp = $fileName1;
            $config_auth->icon_register   = $fileName2;
            $config_auth->icon_password   = $fileName3;
            $config_auth->tmpemilik_id    = $request->tmpemilik_id;
            $config_auth->save();
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $tmconfig = TmConfig::findOrFail($id);
        return $tmconfig;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id'  => 'required|unique:tmconfigs,tmpemilik_id,' . $id,
        ]);

        $icon_otp        = $request->icon_otp;
        $icon_verify_otp = $request->icon_verify_otp;
        $icon_register   = $request->icon_register;
        $icon_password   = $request->icon_password;

        if ($icon_otp != null) {
            $request->validate([
                'icon_otp' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon_otp');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon_otp')->storeAs($this->path, $fileName, 'sftp', 'public');

            $data  = TmConfig::findOrFail($id);
            $exist = $data->icon_otp;
            if ($exist != null) {
                Storage::disk('sftp')->delete($this->path . $exist);
            }

            $config_auth = TmConfig::findOrFail($id);
            $config_auth->update([
                'icon_otp' => $fileName,
                'tmpemilik_id' => $request->tmpemilik_id
            ]);
        }

        if ($icon_verify_otp != null) {
            $request->validate([
                'icon_verify_otp' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file1     = $request->file('icon_verify_otp');
            $fileName1 = time() . "." . $file1->getClientOriginalName();
            $request->file('icon_verify_otp')->storeAs($this->path, $fileName1, 'sftp', 'public');

            $data1  = TmConfig::findOrFail($id);
            $exist1 = $data1->icon_verify_otp;
            if ($exist1 != null) {
                Storage::disk('sftp')->delete($this->path . $exist1);
            }

            $config_auth = TmConfig::findOrFail($id);
            $config_auth->update([
                'icon_verify_otp' => $fileName1,
                'tmpemilik_id' => $request->tmpemilik_id
            ]);
        }

        if ($icon_register != null) {
            $request->validate([
                'icon_register' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file2     = $request->file('icon_register');
            $fileName2 = time() . "." . $file2->getClientOriginalName();
            $request->file('icon_register')->storeAs($this->path, $fileName2, 'sftp', 'public');

            $data2  = TmConfig::findOrFail($id);
            $exist2 = $data2->icon_register;
            if ($exist2 != null) {
                Storage::disk('sftp')->delete($this->path . $exist2);
            }

            $config_auth = TmConfig::findOrFail($id);
            $config_auth->update([
                'icon_register' => $fileName2,
                'tmpemilik_id' => $request->tmpemilik_id
            ]);
        }

        if ($icon_password != null) {
            $request->validate([
                'icon_password' => 'required|mimes:png,jpg,jpeg|max:2024',
            ]);

            $file3     = $request->file('icon_password');
            $fileName3 = time() . "." . $file3->getClientOriginalName();
            $request->file('icon_password')->storeAs($this->path, $fileName3, 'sftp', 'public');

            $data3  = TmConfig::findOrFail($id);
            $exist3 = $data3->icon_password;
            if ($exist3 != null) {
                Storage::disk('sftp')->delete($this->path . $exist3);
            }

            $config_auth = TmConfig::findOrFail($id);
            $config_auth->update([
                'icon_password' => $fileName3,
                'tmpemilik_id' => $request->tmpemilik_id
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $data   = TmConfig::findOrFail($id);
        $exist  = $data->icon_otp;
        $exist1 = $data->icon_verify_otp;
        $exist2 = $data->icon_register;
        $exist3 = $data->icon_password;
        Storage::disk('sftp')->delete($this->path . $exist);
        Storage::disk('sftp')->delete($this->path . $exist1);
        Storage::disk('sftp')->delete($this->path . $exist2);
        Storage::disk('sftp')->delete($this->path . $exist3);
        $data->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
