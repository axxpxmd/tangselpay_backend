<?php

namespace App\Http\Controllers\MasterConfig;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmConfig;
use App\Models\TmPemilik;

class AppController extends Controller
{
    protected $route = 'master-config.app.';
    protected $view  = 'pages.masterConfig.app.';
    protected $title = 'Config App';
    protected $path  = 'assets/icon/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;

        $pemilik = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'pemilik'
        ));
    }

    public function api()
    {
        $app = TmConfig::all();
        return DataTables::of($app)
            ->addColumn('action', function ($p) {
                return "
                    <a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>
                    <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('tmpemilik_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $p->tmpemilik->nama . "</a>";
            })
            ->editColumn('icon',  function ($p) {
                if ($p->icon != null) {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . $this->path . $p->icon . "'>";
                } else {
                    return "<img width='50' class='img-fluid mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'tmpemilik_id', 'icon'])
            ->toJson();
    }

    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;

        $tmconfig = TmConfig::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'tmconfig'
        ));
    }

    public function store(Request $request)
    {
        $request->validate([
            'n_app' => 'required',
            'ket'   => 'required',
            'icon'  => 'required|mimes:png,jpg,jpeg|max:2024',
            'tmpemilik_id' => 'required'
        ]);

        $check = TmConfig::where('tmpemilik_id', $request->tmpemilik_id)->first();
        if ($check != null) {
            $tmpemilik = TmPemilik::where('id', $request->tmpemilik_id)->first();
            return response()->json([
                'message' => 'Pemilik ' . $tmpemilik->nama . ' sudah ada, silahkan di edit.'
            ], 422);
        } else {
            $config_app = new TmConfig();
            $config_app->n_app = $request->n_app;
            $config_app->ket   = $request->ket;
            $config_app->tmpemilik_id = $request->tmpemilik_id;

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            $config_app->icon = $fileName;
            $config_app->save();
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $tmconfig = TmConfig::findOrFail($id);
        return $tmconfig;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tmpemilik_id' => 'required|unique:tmconfigs,tmpemilik_id,' . $id,
            'n_app' => 'required|unique:tmconfigs,n_app,' . $id,
            'ket'   => 'required',
        ]);

        $app   = TmConfig::findOrFail($id);
        $n_app = $request->n_app;
        $ket   = $request->ket;
        $tmpemilik_id = $request->tmpemilik_id;

        if ($request->icon != null) {
            $request->validate([
                'icon'  => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file     = $request->file('icon');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('icon')->storeAs($this->path, $fileName, 'sftp', 'public');

            // Delete from storage
            $exist = $app->icon;
            Storage::disk('sftp')->delete($this->path . $exist);

            $app->update([
                'tmpemilik_id' => $tmpemilik_id,
                'n_app' => $n_app,
                'ket'   => $ket,
                'icon'  => $fileName
            ]);
        } else {
            $app->update([
                'tmpemilik_id' => $tmpemilik_id,
                'n_app' => $n_app,
                'ket'   => $ket,
            ]);
        }

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $app   = TmConfig::findOrFail($id);
        $exist = $app->icon;

        // Delete icon from storage
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from tabel tmconfigs
        $app->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }
}
