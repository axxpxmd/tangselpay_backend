<?php

namespace App\Http\Controllers\LogViewer;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

// Model
use App\Models\TmConfig;
use App\Models\TmPemilik;

class LogsController extends Controller
{
    protected $route = 'logviewer.logs.';
    protected $view  = 'pages.logviewer.';
    protected $title = 'Log Viewer';
    protected $path  = 'assets/icon/';

    protected $jns_log = ["digicash" => "Digicash", "auth" => "Auth", "user" => "User", "validation" => "Validation"];

    public function index(Request $request)
    {
        $route = $this->route;
        $title = $this->title;
        $path  = $this->path;
        $jns_log = $this->jns_log;
        $dt = [];
        $box = [
            "all" => 0,
            "emergency" => 0,
            "alert" => 0,
            "critical" => 0,
            "error" => 0,
            "warning" => 0,
            "notice" => 0,
            "info" => 0,
            "debug" => 0,
        ];

        $f_jns = $request->jns_log ? $request->jns_log : 'digicash';
        $f_tgl = $request->tgl;
        $tgl = $this->getLogFileDates($f_jns);


        if ($f_tgl != '' && $tgl != []) {
            $dt = $this->readLog($f_jns, $f_tgl);
        } elseif ($tgl != [] && $f_tgl == '') {
            $f_tgl = $tgl[0];
            $dt = $this->readLog($f_jns, $f_tgl);
        }

        if ($dt) {
            foreach ($dt["logs"] as $key => $d) {
                $box["all"] = $box["all"] + 1;

                switch (strtolower($d["type"])) {
                    case 'emergency':
                        $box["emergency"] = $box["emergency"] + 1;
                        break;
                    case 'alert':
                        $box["alert"] = $box["alert"] + 1;
                        break;
                    case 'critical':
                        $box["critical"] = $box["critical"] + 1;
                        break;
                    case 'error':
                        $box["error"] = $box["error"] + 1;
                        break;
                    case 'warning':
                        $box["warning"] = $box["warning"] + 1;
                        break;
                    case 'notice':
                        $box["notice"] = $box["notice"] + 1;
                        break;
                    case 'info':
                        $box["info"] = $box["info"] + 1;
                        break;
                    case 'debug':
                        $box["debug"] = $box["debug"] + 1;
                        break;

                    default:
                        dd(strtolower($d["type"]));
                        break;
                }
            }
        }

        // dd($box);




        return view($this->view . 'index', compact(
            'route',
            'title',
            'path',
            'jns_log',
            'tgl',
            'f_jns',
            'f_tgl',
            'dt',
            'box'
        ));
    }



    public function show($id)
    {
        $title = $this->title;
        $route = $this->route;
        $path  = $this->path;

        $tmconfig = TmConfig::findOrFail($id);

        return view($this->view . 'show', compact(
            'title',
            'route',
            'path',
            'tmconfig'
        ));
    }

    public function store(Request $request)
    { }

    public function update(Request $request, $id)
    { }

    public function destroy($id)
    { }

    public function getLogFileDates($jns)
    {
        $dates = [];
        $files = glob(storage_path('logs/api/' . $jns . '/' . $jns . '-*.log'));
        $files = array_reverse($files);
        foreach ($files as $path) {
            $fileName = basename($path);
            preg_match('/(?<=' . $jns . '-)(.*)(?=.log)/', $fileName, $dtMatch);
            $date = $dtMatch[0];
            array_push($dates, $date);
        }

        // return $files;
        return $dates;
    }

    public static function readLog($jns, $tgl)
    {
        $pattern = "/^\[(?<date>.*)\]\s(?<env>\w+)\.(?<type>\w+):(?<message>.*)/m";

        $fileName = $jns . '-' . $tgl . '.log';
        $content = file_get_contents(storage_path('logs/api/' . $jns . '/' . $fileName));
        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER, 0);

        $logs = [];
        foreach ($matches as $match) {
            $logs[] = [
                'timestamp' => $match['date'],
                'env' => $match['env'],
                'type' => $match['type'],
                'message' => trim($match['message'])
            ];
        }

        preg_match('/(?<=laravel-)(.*)(?=.log)/', $fileName, $dtMatch);
        // $date = $dtMatch[0];

        $data = [
            // 'available_log_dates' => $availableDates,
            // 'date' => $date,
            'filename' => $fileName,
            'logs' => $logs
        ];

        return $data;
    }
}
