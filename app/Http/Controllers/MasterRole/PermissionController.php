<?php

namespace App\Http\Controllers\MasterRole;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Model
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    protected $view  = 'pages.masterRoles.permission.';
    protected $title = 'Permission';

    public function index()
    {
        $title = $this->title;

        return view($this->view . 'index', compact('title'));
    }

    public function api()
    {
        $permission = Permission::all();
        return DataTables::of($permission)
            ->addColumn('action', function ($p) {
                return "<a  href='#' onclick='edit(" . $p->id . ")' title='Edit Permission'><i class='icon-pencil mr-1'></i></a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->toJson();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name',
            'guard_name' => 'required'
        ]);

        $input = $request->all();
        Permission::create($input);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function edit($id)
    {
        $permission = Permission::find($id);
        return $permission;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'       => 'required|unique:permissions,name,' . $id,
            'guard_name' => 'required'
        ]);

        $input      = $request->all();
        $permission = Permission::findOrFail($id);
        $permission->update($input);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        Permission::destroy($id);

        return response()->json([
            'message' => 'Data permission berhasil dihapus.'
        ]);
    }
}
