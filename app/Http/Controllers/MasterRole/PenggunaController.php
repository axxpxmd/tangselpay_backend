<?php

namespace App\Http\Controllers\MasterRole;

use Auth;
use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

// Models
use App\User;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\TmPemilik;
use App\Models\AdminDetails;
use App\Models\ModelHasRoles;
use Spatie\Permission\Models\Role;

class PenggunaController extends Controller
{
    protected $route = 'master-role.pengguna.';
    protected $view  = 'pages.masterRoles.pengguna.';
    protected $title = 'Config pengguna';
    protected $path  = 'profile/admin/';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        $roles    = Role::select('id', 'name')->get();
        $provinsi = Provinsi::select('id', 'kode', 'n_provinsi')->get();
        $pemilik  = TmPemilik::select('id', 'nama')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'roles',
            'provinsi',
            'pemilik'
        ));
    }

    public function api()
    {
        $pengguna = AdminDetails::whereNotIn('admin_id', [Auth::user()->id])->get();

        return DataTables::of($pengguna)
            ->addColumn('action', function ($p) {
                return "
                <a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Permission'><i class='icon-remove'></i></a>";
            })
            ->editColumn('admin_id', function ($p) {
                return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary' title='Show Data'>" . $p->admin->username . "</a>";
            })
            ->editColumn('tmpemilik_id', function ($p) {
                return $p->admin->tmpemilik->nama;
            })
            ->editColumn('foto',  function ($p) {
                if ($p->foto != null) {
                    return "<img width='50' height='50' class='img-circular rounded-circle mx-auto d-block' alt='photo' src='" . config('app.sftp_src') . "/profile/admin/" . $p->foto . "'>";
                } else {
                    return "<img width='50' height='50' class='img-circular rounded-circle mx-auto d-block' alt='photo' src='" . asset('images/boy.png') . "'>";
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'foto', 'admin_id', 'tmpemilik_id'])
            ->toJson();
    }

    public function kabupatenByProvinsi($provinsi_id)
    {
        return Kabupaten::select('id', 'n_kabupaten')->where('provinsi_id', $provinsi_id)->get();
    }

    public function kecamatanByKabupaten($kabupaten_id)
    {
        return Kecamatan::select('id', 'n_kecamatan')->where('kabupaten_id', $kabupaten_id)->get();
    }

    public function kelurahanByKecamatan($kecamatan_id)
    {
        return Kelurahan::select('id', 'n_kelurahan')->where('kecamatan_id', $kecamatan_id)->get();
    }

    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:admins',
            'password' => 'required',
            'role_id'  => 'required',
            'nik'      => 'required|min:16',
            'n_admin'  => 'required',
            'email'    => 'required|email|unique:admin_details',
            'telpn'    => 'required|unique:admin_details|max:12',
            'alamat'   => 'required',
            't_lahir'  => 'required',
            'd_lahir'  => 'required',
            'jenis_kelamin' => 'required',
            'pekerjaan'     => 'required',
            'kelurahan_id'  => 'required',
            'foto'          => 'required|mimes:png,jpg,jpeg|max:2024',
            'tmpemilik_id'  => 'required'
        ]);

        /** Tahapan :
         * 1. admins
         * 2. admins_details
         * 3. model_has_roles
         */

        //  Tahap 1
        $username = $request->username;
        $password = $request->password;
        $tmpemilik_id = $request->tmpemilik_id;

        $admin = new User();
        $admin->username = $username;
        $admin->password = Hash::make($password);
        $admin->tmpemilik_id = $tmpemilik_id;
        $admin->save();

        // Tahap 2
        $get_admin_id = User::select('id')->latest()->first();
        $nik     = $request->nik;
        $n_admin = $request->n_admin;
        $email   = $request->email;
        $telpn   = $request->telpn;
        $alamat  = $request->alamat;
        $t_lahir = $request->t_lahir;
        $d_lahir = $request->d_lahir;
        $jenis_kelamin = $request->jenis_kelamin;
        $pekerjaan     = $request->pekerjaan;
        $kelurahan_id  = $request->kelurahan_id;

        $file     = $request->file('foto');
        $fileName = time() . "." . $file->getClientOriginalName();
        $request->file('foto')->storeAs($this->path, $fileName, 'sftp', 'public');

        $admin_detail = new AdminDetails();
        $admin_detail->admin_id = $get_admin_id->id;
        $admin_detail->nik      = $nik;
        $admin_detail->n_admin  = $n_admin;
        $admin_detail->email    = $email;
        $admin_detail->telpn    = $telpn;
        $admin_detail->alamat   = $alamat;
        $admin_detail->t_lahir  = $t_lahir;
        $admin_detail->d_lahir  = $d_lahir;
        $admin_detail->foto     = $fileName;
        $admin_detail->jenis_kelamin = $jenis_kelamin;
        $admin_detail->pekerjaan     = $pekerjaan;
        $admin_detail->kelurahan_id  = $kelurahan_id;
        $admin_detail->save();

        // Tahap 3
        $path    = 'app\User';
        $role_id = $request->role_id;

        $model_has_role = new ModelHasRoles();
        $model_has_role->role_id    = $role_id;
        $model_has_role->model_type = $path;
        $model_has_role->model_id   = $get_admin_id->id;
        $model_has_role->save();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil tersimpan.'
        ]);
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;

        $admin_detail = AdminDetails::findOrFail($id);
        $admin    = User::whereid($admin_detail->admin_id)->first();
        $roles    = Role::select('id', 'name')->get();
        $provinsi = Provinsi::select('id', 'kode', 'n_provinsi')->get();
        $pemilik  = TmPemilik::select('id', 'nama')->get();

        // get role_id by user
        $model_has_role = ModelHasRoles::where('model_id', $admin_detail->admin_id)->first();
        $role  = Role::select('id', 'name')->whereid($model_has_role->role_id)->first();

        return view($this->view . 'show', compact(
            'route',
            'title',
            'admin_detail',
            'admin',
            'roles',
            'role',
            'provinsi',
            'pemilik'
        ));
    }

    public function update(Request $request, $id)
    {
        $admin_detail = AdminDetails::find($id);
        $request->validate([
            'username' => 'required|unique:admins,username,' . $admin_detail->admin_id,
            'role_id'  => 'required',
            'nik'      => 'required|min:16',
            'n_admin'  => 'required',
            'email'    => 'required|email|unique:admin_details,email,' . $id,
            'telpn'    => 'required|unique:admin_details,telpn,' . $id,
            'alamat'   => 'required',
            't_lahir'  => 'required',
            'd_lahir'  => 'required',
            'jenis_kelamin' => 'required',
            'pekerjaan'     => 'required',
            'tmpemilik_id'  => 'required'
        ]);

        /** Tahapan :
         * 1. admins
         * 2. admins_details
         * 3. model_has_roles
         */

        //  Tahap 1
        $username     = $request->username;
        $tmpemilik_id = $request->tmpemilik_id;
        $admin        = User::find($admin_detail->admin_id);
        $admin->update([
            'username'     => $username,
            'tmpemilik_id' => $tmpemilik_id
        ]);

        // Tahap 2
        $nik     = $request->nik;
        $n_admin = $request->n_admin;
        $email   = $request->email;
        $telpn   = $request->telpn;
        $alamat  = $request->alamat;
        $t_lahir = $request->t_lahir;
        $d_lahir = $request->d_lahir;
        $jenis_kelamin = $request->jenis_kelamin;
        $pekerjaan     = $request->pekerjaan;

        if ($request->foto != null) {
            $request->validate([
                'foto' => 'required|mimes:png,jpg,jpeg|max:2024'
            ]);

            $file = $request->file('foto');
            $fileName = time() . "." . $file->getClientOriginalName();
            $request->file('foto')->storeAs($this->path, $fileName, 'sftp', 'public');

            // delete from storage
            $exist = $admin_detail->foto;
            Storage::disk('sftp')->delete($this->path . $exist);

            $admin_detail->update([
                'nik'     => $nik,
                'n_admin' => $n_admin,
                'email'   => $email,
                'telpn'   => $telpn,
                'alamat'  => $alamat,
                't_lahir' => $t_lahir,
                'd_lahir' => $d_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'pekerjaa n'    => $pekerjaan,
                'foto'          => $fileName
            ]);
        } else {
            $admin_detail->update([
                'nik'     => $nik,
                'n_admin' => $n_admin,
                'email'   => $email,
                'telpn'   => $telpn,
                'alamat'  => $alamat,
                't_lahir' => $t_lahir,
                'd_lahir' => $d_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'pekerjaan'     => $pekerjaan,
            ]);
        }

        // Tahap 3
        $role_id = $request->role_id;

        $model_has_role = ModelHasRoles::wheremodel_id($admin_detail->admin_id);
        $model_has_role->update([
            'role_id' => $role_id
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }

    public function destroy($id)
    {
        $admin_detail = AdminDetails::findOrFail($id);

        // delete photo from storage
        $exist = $admin_detail->foto;
        Storage::disk('sftp')->delete($this->path . $exist);

        // delete from table admin_details
        $admin_detail->delete();

        // delete from table model_has_roles
        ModelHasRoles::wheremodel_id($admin_detail->admin_id)->delete();

        // delete from table admins
        User::whereid($admin_detail->admin_id)->delete();

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil dihapus.'
        ]);
    }

    public function editPassword($id)
    {
        $route = $this->route;
        $title = $this->title;

        $admin_detail = AdminDetails::findOrFail($id);

        return view($this->view . 'form_password', compact(
            'route',
            'title',
            'admin_detail'
        ));
    }

    public function updatePassword(Request $request, $id)
    {
        $admin_detail = AdminDetails::find($id);
        $request->validate([
            'password'         => 'required|string|min:8',
            'confirm_password' => 'required|same:password'
        ]);

        $password = $request->password;
        $admin    = User::find($admin_detail->admin_id);
        $admin->update([
            'password' => Hash::make($password)
        ]);

        return response()->json([
            'message' => 'Data ' . $this->title . ' berhasil diperbaharui.'
        ]);
    }
}
