<?php

namespace App\Http\Controllers\MasterTransaksi;

use DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Models\Payment;
use App\Models\TmPemilik;
use App\Models\TmMerchant;
use App\Models\UserDetail;
use App\Models\TmTransaksi;

class TransaksiController extends Controller
{
    protected $route = 'master-transaksi.transaksi.';
    protected $view  = 'pages.masterTransaksi.';
    protected $title = 'Transaksi';

    public function index()
    {
        $route = $this->route;
        $title = $this->title;

        $pemilik  = TmPemilik::select('id', 'nama')->get();
        $merchant = TmMerchant::select('id', 'n_merchant')->get();
        $payment  = Payment::select('id', 'n_payment')->get();

        return view($this->view . 'index', compact(
            'route',
            'title',
            'pemilik',
            'merchant',
            'payment'
        ));
    }

    public function api(Request $request)
    {
        $transaksi   = TmTransaksi::all();
        $pemilik_id  = $request->pemilik_id;
        $merchant_id = $request->merchant_id;
        $payment_id  = $request->payment_id;

        /**
         * Filter By Payment
         */
        if ($payment_id != null) {
            $transaksi = TmTransaksi::select(
                'tmtransaksis.id',
                'tmtransaksis.user_sender_id',
                'tmtransaksis.tmmerchant_id',
                'tmtransaksis.payment_id',
                'tmtransaksis.order_id',
                'tmtransaksis.tgl_transaksi',
                'tmtransaksis.status'
            )
                ->where('payment_id', $payment_id)
                ->get();
        }

        /**
         * Filter By Merchant
         */
        if ($merchant_id != null) {
            $transaksi = TmTransaksi::select(
                'tmtransaksis.id',
                'tmtransaksis.user_sender_id',
                'tmtransaksis.tmmerchant_id',
                'tmtransaksis.payment_id',
                'tmtransaksis.order_id',
                'tmtransaksis.tgl_transaksi',
                'tmtransaksis.status'
            )
                ->where('tmmerchant_id', $merchant_id)
                ->get();

            // Filter By Merchant and payment
            if ($payment_id != null) {
                $transaksi = TmTransaksi::select(
                    'tmtransaksis.id',
                    'tmtransaksis.user_sender_id',
                    'tmtransaksis.tmmerchant_id',
                    'tmtransaksis.payment_id',
                    'tmtransaksis.order_id',
                    'tmtransaksis.tgl_transaksi',
                    'tmtransaksis.status'
                )
                    ->where('tmmerchant_id', $merchant_id)
                    ->where('payment_id', $payment_id)
                    ->get();
            }
        }

        /**
         * Filter By Pemilik
         */
        if ($pemilik_id != null) {
            $transaksi = TmTransaksi::select(
                'tmtransaksis.id',
                'tmtransaksis.user_sender_id',
                'tmtransaksis.tmmerchant_id',
                'tmtransaksis.payment_id',
                'tmtransaksis.order_id',
                'tmtransaksis.tgl_transaksi',
                'tmtransaksis.status'
            )
                ->join('users', 'tmtransaksis.user_sender_id', '=', 'users.id')
                ->join('tmpemiliks', 'users.tmpemilik_id', '=', 'tmpemiliks.id')
                ->where('tmpemiliks.id', $pemilik_id)
                ->get();

            // Filter By Pemilik and Merchant
            if ($merchant_id != null) {
                $transaksi = TmTransaksi::select(
                    'tmtransaksis.id',
                    'tmtransaksis.user_sender_id',
                    'tmtransaksis.tmmerchant_id',
                    'tmtransaksis.payment_id',
                    'tmtransaksis.order_id',
                    'tmtransaksis.tgl_transaksi',
                    'tmtransaksis.status'
                )
                    ->join('users', 'tmtransaksis.user_sender_id', '=', 'users.id')
                    ->join('tmpemiliks', 'users.tmpemilik_id', '=', 'tmpemiliks.id')
                    ->where('tmmerchant_id', $merchant_id)
                    ->where('tmpemiliks.id', $pemilik_id)
                    ->get();
            }

            // Filter By Pemilik and payment
            if ($payment_id != null) {
                $transaksi = TmTransaksi::select(
                    'tmtransaksis.id',
                    'tmtransaksis.user_sender_id',
                    'tmtransaksis.tmmerchant_id',
                    'tmtransaksis.payment_id',
                    'tmtransaksis.order_id',
                    'tmtransaksis.tgl_transaksi',
                    'tmtransaksis.status'
                )
                    ->join('users', 'tmtransaksis.user_sender_id', '=', 'users.id')
                    ->join('tmpemiliks', 'users.tmpemilik_id', '=', 'tmpemiliks.id')
                    ->where('tmpemiliks.id', $pemilik_id)
                    ->where('payment_id', $payment_id)
                    ->get();
            }
        }

        return Datatables::of($transaksi)
            ->addColumn('action', function ($p) {
                return "<a href='#' onclick='remove(" . $p->id . ")' class='text-danger' title='Hapus Role'><i class='icon-remove'></i></a>";
            })
            ->addColumn('pemilik', function ($p) {
                $pemilik = TmPemilik::select('id', 'nama')->where('id', $p->userSender->tmpemilik_id)->first();
                if ($pemilik != null) {
                    return $pemilik->nama;
                } else {
                    return '-';
                }
            })
            ->editColumn('user_sender_id', function ($p) {
                $userDetail = UserDetail::select('id', 'full_name')->where('user_id', $p->user_sender_id)->first();
                if ($userDetail != null) {
                    return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . $userDetail->full_name . "</a>";
                } else {
                    return "<a href='" . route($this->route . 'show', $p->id) . "' class='text-primary ' title='Show Data'>" . '-' . "</a>";
                    // return '-';
                }
            })
            ->editColumn('tmmerchant_id', function ($p) {
                return $p->tmmerchant->n_merchant;
            })
            ->editColumn('payment_id', function ($p) {
                if ($p->tmpayment != null) {
                    return $p->tmpayment->n_payment;
                } else {
                    return '-';
                }
            })
            ->editColumn('status', function ($p) {
                if ($p->status == 1) {
                    return 'Berhasil';
                } else {
                    return 'Tidak';
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'pemilik', 'user_sender_id', 'tmmerchant_id', 'payment_id', 'status'])
            ->toJson();
    }

    public function show($id)
    {
        $route = $this->route;
        $title = $this->title;

        $transaksi  = TmTransaksi::findOrFail($id);
        $pemilik    = TmPemilik::where('id', $transaksi->userSender->tmpemilik_id)->first();
        $userPengirim = UserDetail::select('id', 'full_name')->where('user_id', $transaksi->user_sender_id)->first();
        $userPenerima = UserDetail::select('id', 'full_name')->where('user_id', $transaksi->user_receiver_id)->first();

        return view($this->view . 'show', compact(
            'route',
            'title',
            'transaksi',
            'transaksi',
            'pemilik',
            'userPengirim',
            'userPenerima'
        ));
    }
}
