<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table    = 'user_details';
    protected $fillable = ['id', 'user_id', 'nik', 'u_user', 'full_name', 'first_name', 'last_name', 'email', 'telp', 'jenis_kelamin', 't_lahir', 'd_lahir', 'pekerjaan', 'foto', 'kelurahan_id', 'alamat', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(UserTangselPay::class, 'user_id');
    }

    public function kelurahan_r()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function queryTable()
    {
        $data = UserDetail::select('user_details.id', 'user_details.user_id', 'user_details.full_name', 'user_details.email', 'user_details.telp')
            ->with(['user', 'kelurahan'])
            ->join('users', 'user_details.user_id', '=', 'users.id')
            ->join('tmpemiliks', 'users.tmpemilik_id', '=', 'tmpemiliks.id')
            ->where('tmpemiliks.id', Auth::user()->tmpemilik_id)
            ->get()->take(10);

        return $data;
    }
}
