<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubDetailPayment extends Model
{
    protected $table    = 'tmpayment_sub_details';
    protected $fillable = ['id', 'n_payment_sub_detail', 'ket', 'tmpayment_sub_id', 'no_rek', 'created_at', 'updated_at'];

    public function sub_payment()
    {
        return $this->belongsTo(SubPayment::class, 'tmpayment_sub_id');
    }
}
