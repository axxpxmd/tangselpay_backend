<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table    = 'tmpayments';
    protected $fillable = ['id', 'n_payment', 'ket', 'icon', 'ket', 'created_at', 'updated_at'];
}
