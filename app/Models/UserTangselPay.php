<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTangselPay extends Model
{
    protected $table    = 'users';
    protected $fillable = ['id', 'username', 'password', 'status', 'admin_id', 'tmpemilik_id', 'tmapp_id', 'payment', 'emoney_status', 'api_version', 'device_id', 'device_latitude', 'device_longitude', 'device_type', 'device_os_version', 'device_platform', 'package_version', 'package_build_number', 'tgl_upgrade', 'tokem_fcm', 'remenber_toke', 'version', 'build_number', 'payment_status', 'created_at', 'updated_at'];

    public function pemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }

    public function userDetail()
    {
        return $this->belongsTo(UserDetail::class, 'id', 'user_id')->withDefault([
            'email' => '-',
            'telp' => '-'
        ]);
    }

    public static function queryTable($jenis_kelamin, $bulan, $tahun, $nama, $pemilik)
    {
        $data = UserTangselPay::select('users.id', 'tmpemilik_id', 'jenis_kelamin')
            ->with(['pemilik', 'userDetail'])
            ->has('userDetail')
            ->join('user_details', 'user_details.user_id', '=', 'users.id')
            ->whereYear('users.created_at', $tahun)
            ->whereMonth('users.created_at', $bulan)
            ->where('tmpemilik_id', $pemilik);

        if ($jenis_kelamin != 99) {
            $data->where('user_details.jenis_kelamin', $jenis_kelamin);
        }

        if ($nama != null) {
            $data->where('user_details.full_name', 'like', '%' . $nama . '%');
        }

        return $data->get();
    }
}
