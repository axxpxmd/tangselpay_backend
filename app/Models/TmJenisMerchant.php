<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmJenisMerchant extends Model
{
    protected $table    = 'tmjenis_merchants';
    protected $fillable = ['id', 'n_jenis_merchant', 'created_at', 'updated_at'];
}
