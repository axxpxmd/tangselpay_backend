<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmMerchant_TmPemilik extends Model
{
    protected $table    = 'tmmerchant_tmpemilik';
    protected $fillable = ['id', 'tmpemilik_id', 'tmmerchant_id', 'position', 'created_at', 'updated_at'];

    public function pemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
