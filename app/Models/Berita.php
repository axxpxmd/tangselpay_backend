<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table    = 'tmnews';
    protected $fillable = ['id', 'n_news', 'tmpemilik_id', 'url', 'created_at', 'updated_at'];

    public function tmpemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
