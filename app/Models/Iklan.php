<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table    = 'tmiklans';
    protected $fillable = ['id', 'n_iklan', 'ket', 'image', 'tmpemilik_id', 'created_at', 'updated_at'];

    public function tmpemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
