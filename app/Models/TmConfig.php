<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmConfig extends Model
{
    protected $table    = 'tmconfigs';
    protected $fillable = ['id', 'n_app', 'ket', 'icon', 'color', 'gradient', 'gradient_1', 'gradient_2', 'text_color', 'text_gradient', 'text_gradient_1', 'text_gradient_2', 'icon_otp', 'icon_verify_otp', 'icon_register', 'icon_password', 'gradient_splash', 'color_splash', 'gradient_splash_1', 'gradient_splash_2', 'primary_color', 'button_color', 'background_color', 'tmpemilik_id', 'created_at', 'updated_at'];

    public function tmpemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
