<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmPemilik extends Model
{
    protected $table    = 'tmpemiliks';
    protected $fillable = ['id', 'nama', 'nm_perusahaan', 'no_tlpn', 'logo_text', 'logo', 'alamat', 'alamat_perusahaan', 'server_key', 'created_at', 'updated_at'];
}
