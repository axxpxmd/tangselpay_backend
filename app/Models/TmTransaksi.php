<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmTransaksi extends Model
{
    protected $table    = 'tmtransaksis';
    protected $fillable = ['id', 'user_sender_id', 'user_receiver_id', 'tmmerchant_id', 'order_id', 'status', 'total_bayar', 'biaya', 'tgl_transaksi', 'tgl_update_transaksi', 'tgl_expired', 'payment_id', 'read_status', 'created_at', 'updated_at'];

    public function tmmerchant()
    {
        return $this->belongsTo(TmMerchant::class, 'tmmerchant_id');
    }

    public function userSender()
    {
        return $this->belongsTo(UserTangselPay::class, 'user_sender_id');
    }

    public function userReceiver()
    {
        return $this->belongsTo(UserTangselPay::class, 'user_receiver_id');
    }

    public function tmpayment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }
}
