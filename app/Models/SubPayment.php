<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubPayment extends Model
{
    protected $table    = 'tmpayment_subs';
    protected $fillable = ['id', 'n_payment_sub', 'ket', 'icon', 'tmpayment_id', 'created_at', 'updated_at'];

    public function tmpayment()
    {
        return $this->belongsTo(Payment::class, 'tmpayment_id');
    }
}
