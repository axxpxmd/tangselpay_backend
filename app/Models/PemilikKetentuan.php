<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemilikKetentuan extends Model
{
    protected $table    = 'tmpemilik_ketentuans';
    protected $fillable = ['id', 'tmpemilik_id', 'ketentuan', 'status', 'deskripsi', 'icon', 'created_at', 'updated_at'];

    public function tmpemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
