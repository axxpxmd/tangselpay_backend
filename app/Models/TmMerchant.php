<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmMerchant extends Model
{
    protected $table    = 'tmmerchants';
    protected $fillable = ['id', 'n_merchant', 'ket', 'icon', 'tmjenis_merchant_id', 'status', 'route', 'kode'];

    public function tmtransaksi()
    {
        return $this->hasMany(TmTransaksi::class, 'tmmerchant_id', 'id');
    }

    public function tmmerchant_pemilik()
    {
        return $this->hasMany(TmMerchant_TmPemilik::class, 'tmmerchant_id', 'id');
    }

    public function tmjenis_merchant()
    {
        return $this->belongsTo(TmJenis_Merchants::class, 'tmjenis_merchant_id');
    }
}
