<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Model
use App\User;

class AdminDetails extends Model
{
    protected $table    = 'admin_details';
    protected $fillable = ['id', 'admin_id', 'nik', 'n_admin', 'email', 'telpn', 'alamat', 't_lahir', 'd_lahir', 'jenis_kelamin', 'pekerjaan', 'foto', 'kelurahan_id', 'created_at', 'updated_at'];

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }
}
