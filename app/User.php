<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

// Model
use App\Models\TmPemilik;
use App\Models\AdminDetails;
use App\Models\ModelHasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table    = 'admins';
    protected $fillable = ['id', 'username', 'password', 'status', 'tmpemilik_id', 'remember_token'];
    protected $hidden   = ['remember_token'];
    protected $casts    = ['email_verified_at' => 'datetime'];

    public function model_has_role()
    {
        return $this->hasMany(ModelHasRoles::class, 'model_id', 'id');
    }

    public function admin_detail()
    {
        return $this->hasMany(AdminDetails::class, 'admin_id', 'id');
    }

    public function tmpemilik()
    {
        return $this->belongsTo(TmPemilik::class, 'tmpemilik_id');
    }
}
