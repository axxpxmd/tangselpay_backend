<ul class="sidebar-menu">
    <li class="header"><strong>MAIN NAVIGATION</strong></li>
    <li>
        <a href="{{ route('home') }}">
            <i class="icon icon-dashboard2 blue-text s-18"></i> 
            <span>Dashboard 1</span>
        </a>
    </li>
    <li>
        <a href="{{ route('dashboard2') }}">
            <i class="icon icon-dashboard2 text-success s-18"></i> 
            <span>Dashboard 2</span>
        </a>
    </li>
    @can('master-role')
    <li class="header light"><strong>MASTER ROLES</strong></li>
    <li>
        <a href="{{ route('master-role.role.index') }}">
            <i class="icon icon-key4 amber-text s-18"></i> 
            <span>Role</span>
        </a>
    </li>
    <li class="no-b">
        <a href="{{ route('master-role.permission.index') }}">
            <i class="icon icon-clipboard-list2 text-success s-18"></i> 
            <span>Permission</span>
        </a>
    </li>
    <li class="no-b">
        <a href="{{ route('master-role.pengguna.index') }}">
            <i class="icon icon-users blue-text s-18"></i> 
            <span>Pengguna</span>
        </a>
    </li>
    @endcan
    @can('master-pemilik')
    <li class="header light"><strong>MASTER PEMILIK</strong></li>
    <li>
        <a href="{{ route('master-pemilik.pemilik.index') }}">
            <i class="icon icon-portrait  red-text s-18"></i> 
            <span>Pemilik</span>
        </a>
    </li>
    @endcan
    @can('master-ketentuan')
    <li class="header light"><strong>MASTER KETENTUAN</strong></li>
    <li>
        <a href="{{ route('master-pemilik.pemilikKetentuan.index') }}">
            <i class="icon icon-indeterminate_check_box black-text s-18"></i> 
            <span>Ketentuan</span>
        </a>
    </li>
    @endcan
    @can('master-user')
    <li class="header light"><strong>MASTER USER</strong></li>
    <li>
        <a href="{{ route('master-user.user.index') }}">
            <i class="icon icon-user amber-text s-18"></i> 
            <span class="ml-1">User</span>
        </a>
    </li>
    @endcan
    @can('master-config')
    <li class="header light"><strong>MASTER CONFIGS</strong></li>
    <li class="treeview">
        <a href="{{ route('blank-page') }}">
            <i class="icon icon-settings light-blue-text s-18"></i> 
            <span>Config</span><i class="icon icon-angle-left s-18 pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="{{ route('master-config.app.index') }}">
                    <i class="icon icon-settings2"></i><span>Config App</span>
                </a>
            </li>
            <li>
                <a href="{{ route('master-config.home.index') }}">
                    <i class="icon icon-settings2"></i><span>Config Home</span>
                </a>
            </li>
            <li>
                <a href="{{ route('master-config.auth.index') }}">
                    <i class="icon icon-settings2"></i><span>Config Auth</span>
                </a>
            </li>
            <li>
                <a href="{{ route('master-config.theme.index') }}">
                    <i class="icon icon-settings2"></i><span>Config Theme</span>
                </a>
            </li>
            <li>
                <a href="{{ route('master-config.splash.index') }}">
                    <i class="icon icon-settings2"></i><span>Config Splash</span>
                </a>
            </li>
        </ul>
    </li>
    @endcan
    @can('master-merchant')
    <li class="header light"><strong>MASTER MERCHANTS</strong></li>
    <li>
        <a href="{{ route('master-merchant.merchant.index') }}">
            <i class="icon icon-shop text-success s-18"></i> 
            <span>Merchant</span>
        </a>
        <a href="{{ route('master-merchant.jenisMerchant.index') }}">
            <i class="icon icon-shopping-bag2 purple-text s-18"></i> 
            <span>Jenis Merchant</span>
        </a>
    </li>
    @endcan
    @can('master-payment')
    <li class="header light"><strong>MASTER PAYMENTS</strong></li>
    <li>
        <a href="{{ route('master-payment.payment.index') }}">
            <i class="icon icon-payment text-danger s-18"></i> 
            <span>Payment</span>
        </a>
        <a href="{{ route('master-payment.subPayment.index') }}">
            <i class="icon icon-payment green-text s-18"></i> 
            <span>Sub Payment</span>
        </a>
        <a href="{{ route('master-payment.subDetailPayment.index') }}">
            <i class="icon icon-payment blue-text s-18"></i> 
            <span>Sub Payment Detail</span>
        </a>
    </li>
    @endcan
    @can('master-media')
    <li class="header light"><strong>MASTER MEDIA</strong></li>
    <li>
        <a href="{{ route('master-media.iklan.index') }}">
            <i class="icon icon-tripadvisor purple-text s-18"></i> 
            <span>Iklan</span>
        </a>
        <a href="{{ route('master-media.berita.index') }}">
            <i class="icon icon-newspaper-o amber-text s-18"></i> 
            <span>Berita</span>
        </a>
    </li>
    @endcan
    @can('master-transaksi')
    <li class="header light"><strong>MASTER TRANSAKSI</strong></li>
    <li>
        <a href="{{ route('master-transaksi.transaksi.index') }}">
            <i class="icon icon-dollar red-text s-18"></i> 
            <span class="ml-2">Transaksi</span>
        </a>
    </li>
    @endcan
</ul>
