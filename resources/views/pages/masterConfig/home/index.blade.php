@extends('layouts.app')
@section('title', '| Config Home')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-settings2"></i>
                        List {{ $title }}
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="row">
            <div class="col-md-8">
                <div class="card no-b">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <th>No</th>
                                    <th>Pemilik</th>
                                    <th>Color</th>
                                    <th>Gradient</th>
                                    <th>Gradient 1</th>
                                    <th>Gradient 2</th>
                                    <th>Text Color</th>
                                    <th>Text Gradient</th>
                                    <th>Text Gradient 1</th>
                                    <th>Text Gradient 2</th>
                                    <th></th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="alert"></div>
                <div class="card no-b">
                    <div class="card-body">
                        <form class="needs-validation" id="form" method="POST" enctype="multipart/form-data" novalidate>
                            {{ method_field('POST') }}
                            <input type="hidden" id="id" name="id"/>
                            <h4 id="formTitle">Tambah Data</h4><hr>
                            <div class="form-row form-inline">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label s-12 col-md-4">Pemilik</label>
                                        <div class="col-md-8 p-0 bg-light">
                                            <select class="select2 form-control r-0 light s-12" name="tmpemilik_id" id="tmpemilik_id" autocomplete="off">
                                                <option value="">Pilih</option>
                                                @foreach ($pemilik as $i)
                                                    <option value="{{ $i->id }}">{{ $i->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group m-t-7 color-picker">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Color</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="color" id="color" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="form-group m-t-7">
                                        <label class="col-form-label s-12 col-md-4">Gradient</label>
                                        <div class="col-md-8 p-0 bg-light">
                                            <select class="select2 form-control r-0 light s-12" name="gradient" id="gradient" autocomplete="off">
                                                <option value="">Pilih</option>
                                                <option value="0">Tidak</option>
                                                <option value="1">Ya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group color-picker m-t-7">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Gradient 1</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="gradient_1" id="gradient_1" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="input-group color-picker m-t-7">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Gradient 2</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="gradient_2" id="gradient_2" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="input-group color-picker m-t-7">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Text Color</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="text_color" id="text_color" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="form-group m-t-7">
                                        <label class="col-form-label s-12 col-md-4">Text Gradient</label>
                                        <div class="col-md-8 p-0 bg-light">
                                            <select class="select2 form-control r-0 light s-12" name="text_gradient" id="text_gradient" autocomplete="off">
                                                <option value="">Pilih</option>
                                                <option value="0">Tidak</option>
                                                <option value="1">Ya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group color-picker m-t-7">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Text Gradient 1</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="text_gradient_1" id="text_gradient_1" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="input-group color-picker m-t-7">
                                        <label for="n_app" class="col-form-label s-12 col-md-4">Text Gradient 2</label>
                                        <input type="text" class="form-control r-0 light s-14 col-md-8" name="text_gradient_2" id="text_gradient_2" autocomplete="off" required/>
                                        <span class="input-group-append">
                                            <span class="input-group-text add-on white">
                                                <i class="circle"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="form-group mt-2">
                                        <div class="col-md-4"></div>
                                        <button type="submit" class="btn btn-primary btn-sm" id="action"><i class="icon-save mr-2"></i>Simpan<span id="txtAction"></span></button>
                                        <a class="btn btn-sm" onclick="add()" id="reset">Reset</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card mt-2 no-b">
                    <div class="card-header white">
                        <h4>Tampilan Pada Aplikasi</h4>
                    </div>
                    <div class="card-body">
                        <img class="img-fluid" src="{{ asset('images/config_app.jpg') }}" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table = $('#dataTable').dataTable({
        processing: true,
        serverSide: true,
        order: [ 0, 'asc' ],
        ajax: {
            url: "{{ route($route.'api') }}",
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false, align: 'center', className: 'text-center'},
            {data: 'tmpemilik_id', name: 'tmpemilik_id'},
            {data: 'color', name: 'color', className: 'text-center'},
            {data: 'gradient', name: 'gradient', className: 'text-center'},
            {data: 'gradient_1', name: 'gradient_1', className: 'text-center'},
            {data: 'gradient_2', name: 'gradient_2', className: 'text-center'},
            {data: 'text_color', name: 'text_color', className: 'text-center'},
            {data: 'text_gradient', name: 'text_gradient', className: 'text-center'},
            {data: 'text_gradient_1', name: 'text_gradient_1', className: 'text-center'},
            {data: 'text_gradient_2', name: 'text_gradient_2', className: 'text-center'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    });

    function add(){
        save_method = "add";
        $('#form').trigger('reset');
        $('#formTitle').html('Tambah Data');
        $('input[name=_method]').val('POST');
        $('#txtAction').html('');
        $('#reset').show();
        $('#tmpemilik_id').val("");
        $('#tmpemilik_id').trigger('change.select2');
        $('#color').focus();
    }

    add();
    $('#form').on('submit', function (e) {
        if ($(this)[0].checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else{
            $('#alert').html('');
            // $('#action').attr('disabled', true);
            url = (save_method == 'add') ? "{{ route($route.'store') }}" : "{{ route($route.'update', ':id') }}".replace(':id', $('#id').val());
            $.ajax({
                url : url,
                type : (save_method == 'add') ? 'POST' : 'POST',
                data: new FormData(($(this)[0])),
                contentType: false,
                processData: false,
                success : function(data) {
                    console.log(data);
                    $('#alert').html("<div role='alert' class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Success!</strong> " + data.message + "</div>");
                    table.api().ajax.reload();
                    if(save_method == 'add') add();    
                },
                error : function(data){
                    err = '';
                    respon = data.responseJSON;
                    if(respon.errors){
                        $.each(respon.errors, function( index, value ) {
                            err = err + "<li>" + value +"</li>";
                        });
                    }
                    $('#alert').html("<div role='alert' class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Error!</strong> " + respon.message + "<ol class='pl-3 m-0'>" + err + "</ol></div>");
                }
            });
            return false;
        }
        $(this).addClass('was-validated');
    });

    function edit(id) {
        save_method = 'edit';
        var id = id;
        $('#alert').html('');
        $('#form').trigger('reset');
        $('#formTitle').html("Edit Data <a href='#' onclick='add()' class='btn btn-outline-danger btn-xs pull-right ml-2'>Batal</a>");
        $('#txtAction').html(" Perubahan");
        $('#reset').hide();
        $('input[name=_method]').val('PATCH');
        $.get("{{ route($route.'edit', ':id') }}".replace(':id', id), function(data){
            $('#id').val(data.id);
            $('#tmpemilik_id').val(data.tmpemilik_id);
            $('#tmpemilik_id').trigger('change.select2');
            $('#color').val(data.color).focus();
            $('#gradient').val(data.gradient);
            $('#gradient').trigger('change.select2');
            $('#gradient_1').val(data.gradient_1);
            $('#gradient_2').val(data.gradient_2);
            $('#text_color').val(data.text_color);
            $('#text_gradient').val(data.text_gradient);
            $('#text_gradient').trigger('change.select2');
            $('#text_gradient_1').val(data.text_gradient_1);
            $('#text_gradient_2').val(data.text_gradient_2);
        }, "JSON").fail(function(){
            reload();
        });
    }

    function remove(id){
        $.confirm({
            title: '',
            content: 'Apakah Anda yakin akan menghapus data ini ?',
            icon: 'icon icon-question amber-text',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'red',
            buttons: {
                ok: {
                    text: "ok!",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function(){
                        $.post("{{ route($route.'destroy', ':id') }}".replace(':id', id), {'_method' : 'DELETE'}, function(data) {
                           table.api().ajax.reload();
                            if(id == $('#id').val()) add();
                        }, "JSON").fail(function(){
                            reload();
                        });
                    }
                },
                cancel: function(){}
            }
        });
    }
</script>
@endsection