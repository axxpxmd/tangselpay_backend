@extends('layouts.app')
@section('title', '| Config App')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-settings2"></i>
                        Show {{ $title }} | {{ $tmconfig->tmpemilik->nama }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card">
            <h6 class="card-header"><strong>Pemilik</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $tmconfig->tmpemilik->nama }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama Perusahaan :</strong></label>
                        <label class="col-md-3 s-12">{{ $tmconfig->tmpemilik->nm_perusahaan }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>No Telpon :</strong></label>
                        <label class="col-md-3 s-12">{{ $tmconfig->tmpemilik->no_tlpn }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Alamat :</strong></label>
                        <label class="col-md-5 s-12">{{ $tmconfig->tmpemilik->alamat }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Logo :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src').$path2.$tmconfig->tmpemilik->logo }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <h6 class="card-header"><strong>Aplikasi</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon OTP :</strong></label>
                        <img class="ml-2 " src="{{ config('app.sftp_src').$path.$tmconfig->icon_otp }}" width="100" alt="icon">
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon Verify OTP :</strong></label>
                        <img class="ml-2 " src="{{ config('app.sftp_src').$path.$tmconfig->icon_verify_otp }}" width="100" alt="icon">
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon Registrasi :</strong></label>
                        <img class="ml-2 " src="{{ config('app.sftp_src').$path.$tmconfig->icon_register }}" width="100" alt="icon">
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon Password :</strong></label>
                        <img class="ml-2 " src="{{ config('app.sftp_src').$path.$tmconfig->icon_password }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection