<div class="form-group m-0">
    <label for="" class="col-form-label s-12 col-md-4">
        Icon Password
        <a class="ml-1 mt-1" data-toggle="popover" title="Required" data-html="true" data-content="Max File: 2MB<br/>Format File: (png, jpg, jpeg)<br/>Width: 500 pixel<br/>Height: 500 pixel">
            <i class="icon icon-information2 s-18 red-text"></i>
        </a>
    </label>
    <input type="file" name="icon_password" id="file3" class="input-file3" onchange="tampilkanPreview3(this,'preview3')">
    <label for="file3" class="btn-tertiary js-labelFile3 col-md-8">
        <i class="icon icon-image mr-2 m-b-1"></i>
        <span id="changeText3" class="js-fileName3">Browse Image</span>
    </label>
    <img id="result3" class="d-none" width="150">
    <hr>
    <img width="150" class="rounded img-fluid mt-1 mb-1" style="margin-left: 45%" id="preview3" alt=""/>
</div>
<script>
    (function () {
        'use strict';
        $('.input-file3').each(function () {
            var $input = $(this),
                $label = $input.next('.js-labelFile3'),
                labelVal = $label.html();

            $input.on('change', function (element) {
                var fileName = '';
                if (element.target.value) fileName = element.target.value.split('\\').pop();
                fileName ? $label.addClass('has-file').find('.js-fileName3').html(fileName) : $label
                    .removeClass('has-file').html(labelVal);
            });
        });
    })();

    function tampilkanPreview3(gambar, idpreview) {
        var gb = gambar.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreview);
            var reader = new FileReader();
            if (gbPreview.type.match(imageType)) {
                preview.file = gbPreview;
                reader.onload = (function (element) {
                    return function (e) {
                        element.src = e.target.result;
                    };
                })(preview);
                reader.readAsDataURL(gbPreview);
            } else {
                $.confirm({
                    title: '',
                    content: 'Tipe file tidak boleh! haruf format gambar (png, jpg)',
                    icon: 'icon icon-close',
                    theme: 'modern',
                    closeIcon: true,
                    animation: 'scale',
                    type: 'red',
                    buttons: {
                        ok: {
                            text: "ok!",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: add()
                        }
                    }
                });
            }
        }
    }
</script>