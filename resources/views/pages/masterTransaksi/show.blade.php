@extends('layouts.app')
@section('title', '| Data Transaksi')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-dollar"></i>
                        Show {{ $title }} | {{ $userPengirim->full_name }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card">
            <h6 class="card-header"><strong>Pemilik</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->nama }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama Perusahaan :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->nm_perusahaan }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>No Telpon :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->no_tlpn }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Alamat :</strong></label>
                        <label class="col-md-5 s-12">{{ $pemilik->alamat }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Logo  :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src').'assets/icon/logo/'.$pemilik->logo }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-2">
                    <h6 class="card-header"><strong>Data Transaksi</strong></h6>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Nama Pengirim :</strong></label>
                                <label class="col-md-3 s-12">{{ $userPengirim->full_name }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Nama Penerima :</strong></label>
                                <label class="col-md-3 s-12">{{ $userPenerima != null ? $userPenerima->full_name : '-' }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Merchant :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->tmmerchant->n_merchant }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Order ID :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->order_id }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Status :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->status == 1 ? 'Berhasil' : 'Tidak Berhasil' }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Total Bayar :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->total_bayar }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Biaya :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->biaya }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Tanggal Transaksi :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->tgl_transaksi }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Tanggal Update Transaksi :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->tgl_update_transaksi  }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Tanggal Expired :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->tgl_expired  }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Payment :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->tmpayment != null ? $transaksi->tmpayment->n_payment : '-'  }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-2 text-right s-12"><strong>Status Terbaca :</strong></label>
                                <label class="col-md-3 s-12">{{ $transaksi->read_status == 1 ? 'Sudah Terbaca' : 'Belum Terbaca' }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection