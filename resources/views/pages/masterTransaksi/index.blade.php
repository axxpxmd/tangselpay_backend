@extends('layouts.app')
@section('title', '| Data Transaksi')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-dollar"></i>
                        List {{ $title }}
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card no-b">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="pemilik_id" class="col-form-label s-12 col-md-3 text-right"><strong>Pemilik :</strong></label>
                            <div class="col-sm-6">
                                <select name="pemilik_id" id="pemilik_id" class="select2 form-control r-0 light s-12" onchange="selectOnChange()">
                                    <option value="">Semua</option>
                                    @foreach ($pemilik as $i)
                                    <option value="{{ $i->id }}">{{ $i->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group row" style="margin-top: -10px">
                            <label for="merchant_id" class="col-form-label s-12 col-md-3 text-right"><strong>Merchant :</strong></label>
                            <div class="col-sm-6">
                                <select name="merchant_id" id="merchant_id" class="select2 form-control r-0 light s-12" onchange="selectOnChange()">
                                    <option value="">Semua</option>
                                    @foreach ($merchant as $i)
                                    <option value="{{ $i->id }}">{{ $i->n_merchant }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group row" style="margin-top: -10px">
                            <label for="payment_id" class="col-form-label s-12 col-md-3 text-right"><strong>Payment :</strong></label>
                            <div class="col-sm-6">
                                <select name="payment_id" id="payment_id" class="select2 form-control r-0 light s-12" onchange="selectOnChange()">
                                    <option value="">Semua</option>
                                    @foreach ($payment as $i)
                                    <option value="{{ $i->id }}">{{ $i->n_payment }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="card no-b mt-2">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <th width="30">No</th>
                                    <th>Pemilik</th>
                                    <th>User</th>
                                    <th>Merchant</th>
                                    <th>Order ID</th>
                                    <th>Status</th>
                                    <th>Payment</th>
                                    <th>Tanggal Transaksi</th>
                                    <th></th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table = $('#dataTable').dataTable({
        processing: true,
        serverSide: true,
        order: [ 0, 'asc' ],
        pageLength: 15,
        ajax: {
            url: "{{ route($route.'api') }}",
            method: 'POST',
            data: function (data) {
                data.pemilik_id  = $('#pemilik_id').val();
                data.merchant_id = $('#merchant_id').val();
                data.payment_id  = $('#payment_id').val();
            }
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false, align: 'center', className: 'text-center'},
            {data: 'pemilik', name: 'pemilik'},
            {data: 'user_sender_id', name: 'user_sender_id'},
            {data: 'tmmerchant_id', name: 'tmmerchant_id'},
            {data: 'order_id', name: 'order_id'},
            {data: 'status', name: 'status'},
            {data: 'payment_id', name: 'payment_id'},
            {data: 'tgl_transaksi', name: 'tgl_transaksi'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    });

    function selectOnChange(){
        table.api().ajax.reload();
    }

    function remove(id){
        $.confirm({
            title: '',
            content: 'Apakah Anda yakin akan menghapus data ini ?',
            icon: 'icon icon-question amber-text',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'red',
            buttons: {
                ok: {
                    text: "ok!",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function(){
                        $.post("{{ route($route.'destroy', ':id') }}".replace(':id', id), {'_method' : 'DELETE'}, function(data) {
                           table.api().ajax.reload();
                            if(id == $('#id').val()) add();
                        }, "JSON").fail(function(){
                            reload();
                        });
                    }
                },
                cancel: function(){}
            }
        });
    }

</script>
@endsection
