<div class="d-flex row row-eq-height my-3">
    <div class="col-md-8">
    @include('pages.dashboard.subCard2-1')
    @include('pages.dashboard.subCard2-2')
    <div class="col-md-4">
        <div class="white">
            <div class="card">
                <div class="card-header bg-primary text-white b-b-light">
                    <div class="row justify-content-end">
                        <div class="col">
                            <ul id="myTab4" role="tablist"
                                class="nav nav-tabs card-header-tabs nav-material nav-material-white float-right">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="tab1" data-toggle="tab" href="#v-pills-tab1" role="tab" aria-controls="tab1" aria-expanded="true" aria-selected="true">Transaksi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab2" data-toggle="tab" href="#v-pills-tab2" role="tab" aria-controls="tab2" aria-selected="false">History</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body no-p">
                    <div class="tab-content">
                        <div class="tab-pane animated fadeIn show active" id="v-pills-tab1" role="tabpanel" aria-labelledby="v-pills-tab1">
                            <div class="bg-primary text-white lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14">Jumlah Transaksi</h5>
                                    <span class="s-48 font-weight-lighter text-primary sc-counter">{{ $transaksi }}</span>
                                    <div class="float-right">
                                        <span class="icon icon-money-bag s-48"></span>
                                    </div>
                                </div>
                                <canvas width="378" height="94" data-chart="spark" data-chart-type="line" data-dataset="[[28,530,200,430]]" data-labels="['a','b','c','d']" data-dataset-options="[{ borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'}, ]"></canvas>
                            </div>
                            <div class="card-body p-0 bg-light slimScroll" data-height="347">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr class="no-b">
                                            <th></th>
                                            <th>Nama</th>
                                            <th>Total Transaksi</th>
                                        </tr>
                                        <?php $no = 0;?>
                                        @foreach ($user_transaksi as $i)
                                        <?php $no++ ;?>
                                        <tr>
                                            <td class="text-center">{{ $no }}</td>
                                            <td class="text-uppercase">{{ $i->full_name }}</td>
                                            <td>{{ $i->total_transaksi }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane animated fadeIn" id="v-pills-tab2" role="tabpanel" aria-labelledby="v-pills-tab2">
                            <div class="bg-primary text-white lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14">Total Transaksi</h5>
                                    <span class="s-48 font-weight-lighter text-primary">
                                        <small>Rp. </small><span id="rupiah"></span>
                                    </span>
                                    <div class="float-right mt-2">
                                        <span class="icon icon-dollar s-48"></span>
                                    </div>
                                </div>
                                <canvas width="378" height="94" data-chart="spark" data-chart-type="line" data-dataset="[[620,20,700,50]]" data-labels="['a','b','c','d']" data-dataset-options="[ { borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'}, ]"></canvas>
                            </div>
                           <div class="card-body p-0 bg-light slimScroll" data-height="347">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr class="no-b">
                                            <th></th>
                                            <th>Nama</th>
                                            <th>Merchant</th>
                                            <th>Jumlah</th>
                                        </tr>
                                        <?php $no = 0;?>
                                        @foreach ($history as $i)
                                        <?php $no++ ;?>
                                        <tr>
                                            <td class="text-center">{{ $no }}</td>
                                            <td class="text-uppercase">{{ $i->nama }}</td>
                                            <td>{{ $i->merchant }}</td>
                                            <td>{{ $i->total_bayar  != null ? $i->total_bayar : '0' }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var bilangan = {{ $total_transaksi }}
    
    var	number_string = bilangan.toString(),
        sisa 	= number_string.length % 3,
        rupiah 	= number_string.substr(0, sisa),
        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
    
    // Cetak hasil
    document.getElementById("rupiah").innerHTML = rupiah;
</script>
