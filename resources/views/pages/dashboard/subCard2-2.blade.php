<div class="row row-eq-height">
    <div class="col-md-4">
        <div class="card my-3">
            <div class="card-header white">
                <strong>Merchants</strong>
            </div>
            <div class="card-body slimScroll" data-height="238">
                <ul class="social" style="margin-top: -10px">
                    @foreach ($merchantPemilik as $i)
                    <li>
                        <a class=" mr-3">
                            <img src="{{ config('app.sftp_src') . "assets/icon/merchant/" . $i->icon }}" width="30" alt="test">
                        </a>{{ $i->n_merchant }}
                    </li>
                    @endforeach
                    @if ($check == null)
                    <h5 class="font-weight-normal s-14">Belum Mempunyai Merchant</h5>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card my-3">
            <div class="card-header white">
                <strong> Metode Pembayaran </strong>
            </div>
            <div class="card-body slimScroll" data-height="238">
                <ul class="social" style="margin-top: -10px">
                    @foreach ($metodePembayaran as $i)
                    <li>
                        <a class=" mr-3">
                            <img src="{{ config('app.sftp_src') . "assets/icon/payment/" . $i->icon }}" width="30" alt="test">
                        </a>{{ $i->n_payment }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card my-3">
            <div class="card-header white">
                <strong> Pembayaran </strong>
            </div>
            <div class="card-body slimScroll" data-height="238">
                <ul class="social" style="margin-top: -10px">
                    @foreach ($pembayaran as $i)
                    <li>
                        <a class=" mr-3">
                            <img src="{{ config('app.sftp_src') . "assets/icon/payment/" . $i->icon }}" width="30" alt="test">
                        </a>{{ $i->n_payment_sub }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
