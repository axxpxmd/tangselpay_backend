<div class="card">
    <div class="card-header white">
        <div class="row justify-content-end">
            <div class="col">
                <ul class="nav nav-tabs card-header-tabs nav-material">
                    <li class="nav-item">
                        <a class="nav-link active show" id="w1-tab1" data-toggle="tab" href="#v-pills-w1-tab1">Today</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="w1-tab2" data-toggle="tab" href="#v-pills-w1-tab2">Yesterday</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- -->
    <div class="card-body no-p">
        <div class="tab-content">
            <div class="tab-pane animated fadeInRightShort show active" id="v-pills-w1-tab1" role="tabpanel" aria-labelledby="v-pills-w1-tab1">
                <div class="row p-3">
                    <div class="col-md-6">
                        @include('pages.dashboard.chart')
                    </div>
                    <div class="col-md-6">
                        <div class="card-body pt-0">
                            <div class="my-3">
                                <div class="float-right">
                                    <a href="{{ route('blank-page') }}" class="btn-fab btn-fab-sm btn-primary r-5">
                                        <i class="icon-mail-envelope-closed2 p-0"></i>
                                    </a>
                                    <a href="{{ route('blank-page') }}" class="btn-fab btn-fab-sm btn-success r-5">
                                        <i class="icon-star p-0"></i>
                                    </a>
                                </div>
                                <div class="mr-3 float-left">
                                    <img width="50" class="img-fluid" src="{{ config('app.sftp_src') . "/assets/icon/logo/" . Auth::user()->tmpemilik->logo }}" alt="logo">
                                </div>
                                <div>
                                    <div>
                                        <strong>{{ Auth::user()->tmpemilik->nama }}</strong>
                                    </div>
                                    <small>{{ Auth::user()->tmpemilik->alamat_perusahaan }}</small>
                                </div>
                            </div>
                            @if ($role->name == 'super-admin')
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                    <tr class="no-b">
                                        <th></th>
                                        <th>Nama</th>
                                        <th>Total User</th>
                                    </tr>
                                    <?php $no = 0;?>
                                    @foreach ($userPemilik as $i)
                                    <?php $no++ ;?>
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td class="text-uppercase">{{ $i->nama }}</td>
                                        <td class="sc-counter">{{ $i->total_user }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane animated fadeInLeftShort" id="v-pills-w1-tab2" role="tabpanel" aria-labelledby="v-pills-w1-tab2">
                <p class="p-2">Masih Kosong</p>
            </div>
        </div>
    </div>
</div>
