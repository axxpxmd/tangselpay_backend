@extends('layouts.app')
@section('title', '| Data Merchant')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-shop text-success"></i>
                        Show {{ $title }} | {{ $merchant->n_merchant }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card mt-2">
            <h6 class="card-header"><strong>Data Merchant</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->n_merchant }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Pemilik :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->pemilik->nama }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Jenis Merchant :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->tmjenis_merchant != null ? $merchant->tmjenis_merchant->n_jenis_merchant : '-' }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Status :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->status == 1 ? 'Ya' : 'Tidak' }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Route :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->route }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Posisi :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->position }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Kode :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->kode }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Keterangan :</strong></label>
                        <label class="col-md-3 s-12">{{ $merchant->ket }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src') . $path . $merchant->icon }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection