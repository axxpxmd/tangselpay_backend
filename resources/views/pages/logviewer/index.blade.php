@extends('layouts.app')
@section('title', '| Log Viewer')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-settings2"></i>
                         {{ $title }}
                    </h4>
                </div>
            </div>
        </div>
    </header>

    <div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 1000px;">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_header"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
              </div>
          </div>
        </div>
      </div>

    <div class="container-fluid my-3">
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card no-b">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="tgl">Jenis Log</label>
                                <select class="select2 form-control r-0 light s-12" onchange="getLog(1)" name="jenis_log" id="jenis_log" autocomplete="off">
                                
                                    @foreach ($jns_log as $key => $i)
                                        <option value="{{ $key }}" @if($key == $f_jns) selected @endif >{{ $i }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="tgl">Tanggal</label>
                                <select class="select2 form-control r-0 light s-12" onchange="getLog(2)" name="tgl" id="tgl" autocomplete="off">
                                    @forelse ($tgl as $key => $i)
                                        <option value="{{ $i }}" @if($i == $f_tgl) selected @endif>{{ $i }}</option>
                                   @empty
                                   <option value="">Tidak ada Log</option>
                                    @endforelse
                                
                               
                                </select>
                              
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#8A8A8A">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-list s-48"></span>
                                        </div>
                                        <div class="counter-title">All</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["all"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["all"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["all"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#B71C1C">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-bug s-48"></span>
                                        </div>
                                        <div class="counter-title ">Emergency</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["emergency"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["emergency"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["emergency"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#D32F2F">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-bullhorn s-48"></span>
                                        </div>
                                        <div class="counter-title">Alert</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["alert"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["alert"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["alert"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                
                        <div class="row my-3">
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#F44336">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-heartbeat s-48"></span>
                                        </div>
                                        <div class="counter-title">Critical</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["critical"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["critical"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["critical"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#FF5722">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-times-circle s-48"></span>
                                        </div>
                                        <div class="counter-title">Error</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["error"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["error"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["error"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#FF9100">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-exclamation-triangle s-48"></span>
                                        </div>
                                        <div class="counter-title">Warning</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["warning"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["warning"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["warning"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#4CAF50">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-exclamation-circle s-48"></span>
                                        </div>
                                        <div class="counter-title">Notice</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["notice"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["notice"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["notice"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#1976D2">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-info-circle s-48"></span>
                                        </div>
                                        <div class="counter-title">Info</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["info"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["info"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["info"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="counter-box text-white  r-5 p-3" style="background-color:#90CAF9">
                                    <div class="p-4">
                                        <div class="float-right">
                                            <span class="fa fa-fw fa-life-ring s-48"></span>
                                        </div>
                                        <div class="counter-title">Debug</div>
                                        <h5 class="sc-counter mt-3 ">{{ $box["debug"] }}</h5>
                                    </div>
                                    <div class="progress progress-xs r-0">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $box["all"] == 0 ? 0 : ($box["debug"]/$box['all'])*100}}%;" aria-valuenow="{{ $box["all"] == 0 ? 0 : ($box["debug"]/$box['all'])*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="responsive_table">
                            <table class="table table-hover" id="dataTable">
                                <thead>
                                <tr>
                                    <td width="140">Timestamp</td>
                                    <td width="120">Env</td>
                                    <td width="120">Type</td>
                                    <td>Message</td>
                                    <td>Action</td>
                                </tr>
                                </thead>

                                <tbody>
                                    @if ($dt)
                                        
                                   
                                    @forelse ($dt["logs"] as $key => $d)
                                    <tr>
                                    <td>{{ $d["timestamp"] }}</td>
                                    <td>{{ $d["env"] }}</td>
                                    <td><span class="badge {{ strtolower($d["type"])}}">{{ $d["type"] }}</span></td>
                                    <td>{{ substr($d["message"],0,75) }} ...</td>
                                    <td><a data-toggle="modal" data-target=".bd-example-modal-lg" href="#modal" data-id="{{$key}}">Detail</a></td>
                                    </tr>
                                    @empty
                                        
                                    @endforelse
                                    @endif
                                </tbody>
                
                              
                            </table>
                        </div>
                    </div>
                </div>
            </div>
           
          
            
        </div>

      
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function () {
    $('#modal').on('show.bs.modal', function (event) {
       var arrayID = $(event.relatedTarget).attr('data-id');
       $('#modal_header').html('LOG '+js_array[arrayID]["timestamp"]);
   
       
      let type = js_array[arrayID]["type"];

      let content =  `<table width="100%" style="font-size:14px">
                    <tr>
                        <td>Tanggal</td>
                        <td>: </td>
                        <td>`+js_array[arrayID]["timestamp"]+`</td>
                    </tr>
                    <tr>
                        <td>Env</td>
                        <td>: </td>
                        <td>`+js_array[arrayID]["env"]+`</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>: </td>
                        <td> <span id="type_modal" class="badge `+type.toLowerCase()+`">`+js_array[arrayID]["type"]+`</span></td>
                    </tr>
                    <tr>
                        <td>Detail</td>
                        <td>: </td>
                        <td>`+js_array[arrayID]["message"]+`</td>
                    </tr>
                </table>`
                $('.modal-body').html(content);        
    });
});

$('#dataTable').dataTable({
    order: [ 0, 'desc' ],
});

function getLog(f){
    jns = $('#jenis_log').val();
    tgl = $('#tgl').val();
    if(f == 1){
        tgl = '';
    }
    window.location = "{{ route($route.'index')}}"+'?jns_log='+jns+'&tgl='+tgl ;
}
@if ($dt)
var js_array ={!!json_encode($dt["logs"] )!!} ;
@endif
   
</script>
@endsection