@extends('layouts.app')
@section('title', '| Log Viewer')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-settings2"></i>
                        Dashboard {{ $title }}
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="row my-3">
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#8A8A8A">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-list s-48"></span>
                        </div>
                        <div class="counter-title">All</div>
                        <h5 class="sc-counter mt-3 counter-animated">1,228</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#B71C1C">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-bug s-48"></span>
                        </div>
                        <div class="counter-title ">Emergency</div>
                        <h5 class="sc-counter mt-3 counter-animated">1,228</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#D32F2F">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-bullhorn s-48"></span>
                        </div>
                        <div class="counter-title">Alert</div>
                        <h5 class="sc-counter mt-3 counter-animated">1,228</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row my-3">
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#F44336">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-heartbeat s-48"></span>
                        </div>
                        <div class="counter-title">Critical</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#FF5722">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-times-circle s-48"></span>
                        </div>
                        <div class="counter-title">Error</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#FF9100">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-exclamation-triangle s-48"></span>
                        </div>
                        <div class="counter-title">Warning</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#4CAF50">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-exclamation-circle s-48"></span>
                        </div>
                        <div class="counter-title">Notice</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#1976D2">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-info-circle s-48"></span>
                        </div>
                        <div class="counter-title">Info</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="counter-box text-white  r-5 p-3" style="background-color:#90CAF9">
                    <div class="p-4">
                        <div class="float-right">
                            <span class="fa fa-fw fa-life-ring s-48"></span>
                        </div>
                        <div class="counter-title">Debug</div>
                        <h5 class="sc-counter mt-3 counter-animated">550</h5>
                    </div>
                    <div class="progress progress-xs r-0">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   
</script>
@endsection