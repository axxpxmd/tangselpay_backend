<style>
    /* #pieChart {
        height: 300px !important
    } */
</style>
<figure class="highcharts-figure">
    <div id="pieChart"></div>
</figure>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
    var year =  new Date().getFullYear();

    Highcharts.chart('pieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        credits: {
            enabled: false
        },
        title: {
            text: "{{ $year }}"
        },
        tooltip: {
            pointFormat: '{series.name}<b>{point.y}</b> | {point.percentage:.1f} %'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Total Perempuan',
                y: {{ $totalPerempuan }},
                sliced: true,
                selected: true,
                color: '#FFCE3C',
            }, {
                name: 'Total Laki',
                y: {{ $totalLaki }},
                color: '#4385F4',
            }]
        }]
    });
</script>
