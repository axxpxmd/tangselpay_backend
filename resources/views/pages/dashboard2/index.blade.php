@extends('layouts.app')
@section('title', '| Dashboard  ')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-dashboard"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="tab-content pb-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                <div class="card my-2 mx-1">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="font-weight-bold">Tahun</label>
                                    <select name="tahun" id="tahun_filter" class="select2 form-control r-0 light s-12">
                                        <option value="2020" {{ $year == 2020 ? 'selected' : '' }}>2020</option>
                                        <option value="2021" {{ $year == 2021 ? 'selected' : '' }}>2021</option>
                                        <option value="2022" {{ $year == 2022 ? 'selected' : '' }}>2022</option>
                                        <option value="2023" {{ $year == 2023 ? 'selected' : '' }}>2023</option>
                                        <option value="2024" {{ $year == 2024 ? 'selected' : '' }}>2024</option>
                                    </select>
                                </div>
                                <a onclick="filter()" class="btn btn-sm btn-primary"><i class="icon icon-filter"></i>Filter</a>
                            </div>
                            <div class="col-md-8 border-black">
                                <div class="row justify-content-center">
                                    <div class="mr-4" style="border-left: 3px #F3F5F8 solid; height: 125px; margin-left: -100px !important"></div>
                                    @foreach ($pemiliks as $key => $i)
                                        @php
                                            $color = ['#26a69a', '#26c6da', '#42a5f5', '#ef5350', '#ff7043', '#5c6bc0', '#ffee58', '#bdbdbd', '#66bb6a ', '#ec407a'];
                                        @endphp
                                        <div class="mx-3 mt-2 mb-2 rounded-circle p-4" style="background: #F3F5F8" title="{{ $i->nama }}">
                                            <img class="img-circular" height="60" width="60" src="{{ config('app.sftp_src') . "assets/icon/logo/" . $i->logo }}" alt="{{ $i->nama }}">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-1">
                    <div class="col-sm-4 p-0">
                        <div class="card mr-2">
                            <h6 class="card-header font-weight-bold">Total Pengguna</h6>
                            <div class="card-body">
                                @include('pages.dashboard2.drilldown')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-0">
                        <div class="card mr-2">
                            <h6 class="card-header font-weight-bold">Total Gender</h6>
                            <div class="card-body">
                                @include('pages.dashboard2.pieChart')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-0">
                        <div class="white">
                            <div class="card">
                                <div class="card-header bg-primary text-white b-b-light">
                                    <div class="row justify-content-end">
                                        <div class="col">
                                            <ul id="myTab4" role="tablist"
                                                class="nav nav-tabs card-header-tabs nav-material nav-material-white float-right">
                                                <li class="nav-item">
                                                    <a class="nav-link active show" id="tab1" data-toggle="tab" href="#v-pills-tab1" role="tab" aria-controls="tab1" aria-expanded="true" aria-selected="true">Transaksi</a>
                                                </li>
                                                {{-- <li class="nav-item">
                                                    <a class="nav-link" id="tab2" data-toggle="tab" href="#v-pills-tab2" role="tab" aria-controls="tab2" aria-selected="false">History</a>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body no-p">
                                    <div class="tab-content">
                                        <div class="tab-pane animated fadeIn show active" id="v-pills-tab1" role="tabpanel" aria-labelledby="v-pills-tab1">
                                            <div class="bg-primary text-white lighten-2">
                                                <div class="pt-5 pb-2 pl-5 pr-5">
                                                    <h5 class="font-weight-normal s-14">Total Transaksi {{ $year }}</h5>
                                                    <span class="fs-40 font-weight-lighter text-primary">{{ number_format($totalTransaksi) }}</span>
                                                    <div class="float-right">
                                                        <span class="icon icon-money-bag s-48"></span></span>
                                                    </div>
                                                </div>
                                                <canvas width="278" height="1" data-chart="spark" data-chart-type="line" data-dataset="[[28,530,200,430]]" data-labels="['a','b','c','d']" data-dataset-options="[{ borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'}, ]"></canvas>
                                            </div>
                                            <div class="card-body p-0 bg-white slimScroll" data-height="310">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                        <tr class="no-b">
                                                            <th></th>
                                                            <th>Nama</th>
                                                            <th>Total Transaksi</th>
                                                        </tr>
                                                        @foreach ($listTotalTransaksi as $key => $i)
                                                        <tr>
                                                            <td class="text-center">{{ $key+1 }}</td>
                                                            <td class="text-uppercase">{{ $i->tahun }}</td>
                                                            <td>{{ number_format($i->total_bayar) }}</td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="tab-pane animated fadeIn" id="v-pills-tab2" role="tabpanel" aria-labelledby="v-pills-tab2">
                                            <div class="bg-primary text-white lighten-2">
                                                <div class="pt-5 pb-2 pl-5 pr-5">
                                                    <h5 class="font-weight-normal s-14">Total Transaksi</h5>
                                                    <span class="s-48 font-weight-lighter text-primary">
                                                        <small>Rp. </small><span id="rupiah"></span>
                                                    </span>
                                                    <div class="float-right mt-2">
                                                        <span class="icon icon-dollar s-48"></span>
                                                    </div>
                                                </div>
                                                <canvas width="278" height="1" data-chart="spark" data-chart-type="line" data-dataset="[[620,20,700,50]]" data-labels="['a','b','c','d']" data-dataset-options="[ { borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'}, ]"></canvas>
                                            </div>
                                           <div class="card-body p-0 bg-white slimScroll" data-height="310">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                        <tr class="no-b">
                                                            <th></th>
                                                            <th>Nama</th>
                                                            <th>Merchant</th>
                                                            <th>Jumlah</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">1</td>
                                                            <td class="text-uppercase"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-1 mt-2">
                    <div class="col-sm-7 p-0">
                        <div class="card mr-2">
                            <h6 class="card-header font-weight-bolder">Total Transaksi Merchant {{ $year }}</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <th>No</th>
                                            <th>Merchant</th>
                                            <th>Total Transaksi</th>
                                            <th>Total Bayar</th>
                                        </thead>
                                        <tbody>
                                            @forelse ($transaksiMerchant as $key => $i)
                                                <tr>
                                                    <td class="text-center">{{ $key+1 }}</td>
                                                    <td>{{ $i->tmmerchant->n_merchant }}</td>
                                                    <td>{{ $i->total_transaksi }}</td>
                                                    <td>Rp. {{ number_format($i->total_bayar) }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td class="text-center" colspan="4">Tidak ada data.</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 p-0">
                        <div class="card">
                            <div class="card-body">
                                card 3
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function filter(){
        var tahun = $('#tahun_filter').val();

        window.location.href = "{{ route('dashboard2') }}?tahun="+tahun;
    }
</script>
@endsection
