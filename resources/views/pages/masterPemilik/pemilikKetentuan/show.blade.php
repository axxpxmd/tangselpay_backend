@extends('layouts.app')
@section('title', '| Data Pemilik Ketentuan')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-indeterminate_check_box"></i>
                        Show {{ $title }} | {{ $pemilikKetentuan->ketentuan }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card">
            <h6 class="card-header"><strong>Pemilik</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilikKetentuan->tmpemilik->nama }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama Perusahaan :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilikKetentuan->tmpemilik->nm_perusahaan }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>No Telpon :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilikKetentuan->tmpemilik->no_tlpn }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Alamat :</strong></label>
                        <label class="col-md-5 s-12">{{ $pemilikKetentuan->tmpemilik->alamat }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Logo  :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src').'/assets/icon/logo/'.$pemilikKetentuan->tmpemilik->logo }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <h6 class="card-header"><strong>Aplikasi</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Ketentuan :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilikKetentuan->ketentuan }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Deskripsi :</strong></label>
                        <div class="card col-md-8 p-0">
                            <div class="card-body">
                                <label class="col-md-3 s-12">{!! $pemilikKetentuan->deskripsi !!}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src') . $path . $pemilikKetentuan->icon }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection