@extends('layouts.app')
@section('title', '| Data Pemilik')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-portrait"></i>
                        Show {{ $title }} | {{ $tmpemilik->nama }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul role="tablist" class="nav nav-material nav-material-white responsive-tab">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid">
        <div class="tab-content my-3" id="pills-tabContent">
            <div class="tab-pane fade show active" id="semua-data" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <h6 class="card-header"><strong>Data Pemilik</strong></h6>
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                                        <label class="col-md-3 s-12">{{ $tmpemilik->nama }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>No telpn :</strong></label>
                                        <label class="col-md-3 s-12">{{ $tmpemilik->no_tlpn }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Nama Perusahaan :</strong></label>
                                        <label class="col-md-3 s-12">{{ $tmpemilik->nm_perusahaan }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Logo Text :</strong></label>
                                        <label class="col-md-3 s-12">{{ $tmpemilik->logo_text }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Alamat :</strong></label>
                                        <label class="col-md-5 s-12">{{ $tmpemilik->alamat }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Alamat Perusahaan :</strong></label>
                                        <label class="col-md-5 s-12">{{ $tmpemilik->alamat_perusahaan }}</label>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-2 text-right s-12"><strong>Logo :</strong></label>
                                        @if ($tmpemilik->logo != null)
                                        <img class="ml-2 m-t-7 rounded" src="{{ config('app.sftp_src') . $path . $tmpemilik->logo }}" width="100" alt="icon">
                                        @else
                                        <img class="ml-2 m-t-7 rounded-circle" src="{{ asset('images/boy.png') }}" width="100" alt="icon">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
