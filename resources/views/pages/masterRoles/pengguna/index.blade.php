@extends('layouts.app')
@section('title', '| Data Pengguna')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon-users"></i>
                        Data {{ $title }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul role="tablist" class="nav nav-material nav-material-white responsive-tab">
                    <li class="nav-item">
                        <a class="nav-link active show" id="tab1" data-toggle="tab" href="#semua-data" role="tab"><i class="icon icon-home2"></i>Semua Data</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tambah-data" role="tab"><i class="icon icon-plus"></i>Tambah Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid">
        <div class="tab-content my-3" id="pills-tabContent">
            <div class="tab-pane fade show active" id="semua-data" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <th width="30">No</th>
                                            <th>Username</th>
                                            <th>Pemilik</th>
                                            <th>Nama</th>
                                            <th>NIK</th>
                                            <th>Email</th>
                                            <th>No Telp</th>
                                            <th width="60">Foto</th>
                                            <th></th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tambah-data" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div id="alert"></div>
                        <div class="card">
                            <div class="card-body">
                                <form class="needs-validation" id="form" method="POST"  enctype="multipart/form-data" novalidate>
                                    {{ method_field('POST') }}
                                    <input type="hidden" id="id" name="id"/>
                                    <h4 id="formTitle">Tambah Data</h4><hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-row form-inline">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label s-12 col-md-2">Pemilik</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="tmpemilik_id" id="tmpemilik_id" autocomplete="off">
                                                                <option value="">Pilih</option>
                                                                @foreach ($pemilik as $i)
                                                                    <option value="{{ $i->id }}">{{ $i->nama }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-7">
                                                        <label for="username" class="col-form-label s-12 col-md-2">Username</label>
                                                        <input type="text" name="username" id="username" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="password" class="col-form-label s-12 col-md-2">Password</label>
                                                        <input type="password" name="password" id="password" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-form-label s-12 col-md-2">Role</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="role_id" id="role_id" autocomplete="off">
                                                                <option value="">Pilih</option>
                                                                @foreach ($roles as $i)
                                                                    <option value="{{ $i->id }}">{{ $i->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-5">
                                                        <label for="nik" class="col-form-label s-12 col-md-2">NIK</label>
                                                        <input type="number" name="nik" id="nik" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="nama" class="col-form-label s-12 col-md-2">Nama</label>
                                                        <input type="text" name="n_admin" id="n_admin" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="email" class="col-form-label s-12 col-md-2">Email</label>
                                                        <input type="email" name="email" id="email" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="telpn" class="col-form-label s-12 col-md-2">No Telp</label>
                                                        <input type="text" name="telpn" id="telpn" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="nama" class="col-form-label s-12 col-md-2">Alamat</label>
                                                        <input type="text" name="alamat" id="alamat" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="t_lahir" class="col-form-label s-12 col-md-2">Tempat Lahir</label>
                                                        <input type="text" name="t_lahir" id="t_lahir" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label for="d_lahir" class="col-form-label s-12 col-md-2">Tanggal Lahir</label>
                                                        <input type="text" name="d_lahir" id="d_lahir" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label class="col-form-label s-12 col-md-2">Jenis Kelamin</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="jenis_kelamin" id="jenis_kelamin" autocomplete="off">
                                                                <option value="">Pilih</option>
                                                                <option value="laki">Laki-Laki</option>
                                                                <option value="perempuan">Perempuan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-row form-inline">
                                                <div class="col-md-12">
                                                    <div class="form-group m-t-5">
                                                        <label for="pekerjaan" class="col-form-label s-12 col-md-2">Pekerjaan</label>
                                                        <input type="text" name="pekerjaan" id="pekerjaan" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label class="col-form-label s-12 col-md-2">Provinsi</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="provinsi_id" id="provinsi_id" autocomplete="off">
                                                                <option value="">Pilih</option>
                                                                @foreach ($provinsi as $i)
                                                                    <option value="{{ $i->id }}">{{ $i->n_provinsi }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-5">
                                                        <label class="col-form-label s-12 col-md-2">Kabupaten/Kota</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="kabupaten_id" id="kabupaten_id" autocomplete="off">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-5">
                                                        <label class="col-form-label s-12 col-md-2">Kecamatan</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="kecamatan_id" id="kecamatan_id" autocomplete="off">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-5">
                                                        <label class="col-form-label s-12 col-md-2">Kelurahan</label>
                                                        <div class="col-md-8 p-0 bg-light">
                                                            <select class="select2 form-control r-0 light s-12" name="kelurahan_id" id="kelurahan_id" autocomplete="off">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mt-1">
                                                        <label for="" class="col-form-label s-12 col-md-2">
                                                            Foto
                                                            <a class="ml-1 mt-1" data-toggle="popover" title="Required" data-html="true" data-content="Max File: 2MB<br/>Format File: (png, jpg, jpeg)<br/>Width: 500 pixel<br/>Height: 500 pixel">
                                                                <i class="icon icon-information2 s-18 red-text"></i>
                                                            </a>
                                                        </label>
                                                        <input type="file" name="foto" id="file" class="input-file" onchange="tampilkanPreview(this,'preview')">
                                                        <label for="file" class="btn-tertiary js-labelFile col-md-8">
                                                            <i class="icon icon-image mr-2 m-b-1"></i>
                                                            <span id="changeText" class="js-fileName">Browse Image</span>
                                                        </label>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <label class="col-form-label s-12 col-md-2"></label>
                                                        <img width="150" class="rounded img-fluid mt-2 mb-2" id="preview" alt=""/>
                                                    </div>
                                                    <div class="form-group m-0">
                                                        <div class="col-md-2"></div>
                                                        <button type="submit" class="btn btn-primary btn-sm" id="action"><i class="icon-save mr-2"></i>Simpan<span id="txtAction"></span></button>
                                                        <a class="btn btn-sm" onclick="add()" id="reset">Reset</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table = $('#dataTable').dataTable({
        processing: true,
        serverSide: true,
        order: [ 0, 'asc' ],
        ajax: {
            url: "{{ route($route.'api') }}",
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false, align: 'center', className: 'text-center'},
            {data: 'admin_id', name: 'admin_id'},
            {data: 'tmpemilik_id', name: 'tmpemilik_id'},
            {data: 'n_admin', name: 'n_admin'},
            {data: 'nik', name: 'nik'},
            {data: 'email', name: 'email'},
            {data: 'telpn', name: 'telpn'},
            {data: 'foto', name: 'foto'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    });

    $('#d_lahir').datetimepicker({
        format:'Y-m-d',
        onShow:function( ct ){},
        timepicker:false
    });

    (function () {
        'use strict';
        $('.input-file').each(function () {
            var $input = $(this),
                $label = $input.next('.js-labelFile'),
                labelVal = $label.html();

            $input.on('change', function (element) {
                var fileName = '';
                if (element.target.value) fileName = element.target.value.split('\\').pop();
                fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label
                    .removeClass('has-file').html(labelVal);
            });
        });
    })();

    function tampilkanPreview(gambar, idpreview) {
        var gb = gambar.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreview);
            var reader = new FileReader();
            if (gbPreview.type.match(imageType)) {
                preview.file = gbPreview;
                reader.onload = (function (element) {
                    return function (e) {
                        element.src = e.target.result;
                    };
                })(preview);
                reader.readAsDataURL(gbPreview);
            } else {
                $.confirm({
                    title: '',
                    content: 'Tipe file tidak boleh! haruf format gambar (png, jpg, jpeg)',
                    icon: 'icon icon-close',
                    theme: 'modern',
                    closeIcon: true,
                    animation: 'scale',
                    type: 'red',
                    buttons: {
                        ok: {
                            text: "ok!",
                            btnClass: 'btn-primary',
                            keys: ['enter'],
                            action: add()
                        }
                    }
                });
            }
        }
    }

    $('#provinsi_id').on('change', function(){
        val = $(this).val();
        option = "<option value=''>&nbsp;</option>";
        if(val == ""){
            $('#kabupaten_id').html(option);
            $('#kecamatan_id').html(option);
            $('#kelurahan_id').html(option);
            selectOnChange();
        }else{
            $('#kabupaten_id').html("<option value=''>Loading...</option>");
            url = "{{ route($route.'kabupatenByProvinsi', ':id') }}".replace(':id', val);
            $.get(url, function(data){
                if(data){
                    $.each(data, function(index, value){
                        option += "<option value='" + value.id + "'>" + value.n_kabupaten +"</li>";
                    });
                    $('#kabupaten_id').empty().html(option);

                    $("#kabupaten_id").val($("#kabupaten_id option:first").val()).trigger("change.select2");
                }else{
                    $('#kabupaten_id').html(option);
                    $('#kecamatan_id').html(option);
                    $('#kelurahan_id').html(option);
                    selectOnChange();
                }
            }, 'JSON');
        }
    });

    $('#kabupaten_id').on('change', function(){
        val = $(this).val();
        option = "<option value=''>&nbsp;</option>";
        if(val == ""){
            $('#kecamatan_id').html(option);
            $('#kelurahan_id').html(option);
            selectOnChange();
        }else{
            $('#kecamatan_id').html("<option value=''>Loading...</option>");
            url = "{{ route($route.'kecamatanByKabupaten', ':id') }}".replace(':id', val);
            $.get(url, function(data){
                if(data){
                    $.each(data, function(index, value){
                        option += "<option value='" + value.id + "'>" + value.n_kecamatan +"</li>";
                    });
                    $('#kecamatan_id').empty().html(option);

                    $("#kecamatan_id").val($("#kecamatan_id option:first").val()).trigger("change.select2");
                }else{
                    $('#kecamatan_id').html(option);
                    $('#kelurahan_id').html(option);
                    selectOnChange();
                }
            }, 'JSON');
        }
    });

    $('#kecamatan_id').on('change', function(){
        val = $(this).val();
        option = "<option value=''>&nbsp;</option>";
        if(val == ""){
            $('#kelurahan_id').html(option);
            selectOnChange();
        }else{
            $('#kelurahan_id').html("<option value=''>Loading...</option>");
            url = "{{ route($route.'kelurahanByKecamatan', ':id') }}".replace(':id', val);
            $.get(url, function(data){
                if(data){
                    $.each(data, function(index, value){
                        option += "<option value='" + value.id + "'>" + value.n_kelurahan +"</li>";
                    });
                    $('#kelurahan_id').empty().html(option);

                    $("#kelurahan_id").val($("#kelurahan_id option:first").val()).trigger("change.select2");
                }else{
                    $('#kelurahan_id').html(option);
                    selectOnChange();
                }
            }, 'JSON');
        }
    });

    function selectOnChange(){
        ajax.reload();
    }

    function add(){
        save_method = "add";
        $('#form').trigger('reset');
        $('#formTitle').html('Tambah Data');
        $('input[name=_method]').val('POST');
        $('#txtAction').html('');
        $('#reset').show();
        $('#preview').attr({ 'src': '-', 'alt': ''});
        $('#changeText').html('Browse Image')
        $('#username').focus();
    }

    $('#form').on('submit', function (e) {
        if ($(this)[0].checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else{
            $('#alert').html('');
            url = "{{ route($route.'store') }}",
            $.ajax({
                url : url,
                type : 'POST',
                data: new FormData(($(this)[0])),
                contentType: false,
                processData: false,
                success : function(data) {
                    console.log(data);
                    $('#alert').html("<div role='alert' class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Success!</strong> " + data.message + "</div>");
                    table.api().ajax.reload();
                    add();    
                },
                error : function(data){
                    err = '';
                    respon = data.responseJSON;
                    if(respon.errors){
                        $.each(respon.errors, function( index, value ) {
                            err = err + "<li>" + value +"</li>";
                        });
                    }
                    $('#alert').html("<div role='alert' class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Error!</strong> " + respon.message + "<ol class='pl-3 m-0'>" + err + "</ol></div>");
                }
            });
            return false;
        }
        $(this).addClass('was-validated');
    });

    function remove(id){
        $.confirm({
            title: '',
            content: 'Apakah Anda yakin akan menghapus data ini ?',
            icon: 'icon icon-question amber-text',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'red',
            buttons: {
                ok: {
                    text: "ok!",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function(){
                        $.post("{{ route($route.'destroy', ':id') }}".replace(':id', id), {'_method' : 'DELETE'}, function(data) {
                           table.api().ajax.reload();
                            if(id == $('#id').val()) add();
                        }, "JSON").fail(function(){
                            reload();
                        });
                    }
                },
                cancel: function(){}
            }
        });
    }

</script>
@endsection
