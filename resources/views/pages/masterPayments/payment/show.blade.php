@extends('layouts.app')
@section('title', '| Data Payment')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-payment"></i>
                        Show {{ $title }} | {{ $payment->n_payment }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card mt-2">
            <h6 class="card-header"><strong>Aplikasi</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $payment->n_payment }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Route :</strong></label>
                        <label class="col-md-3 s-12">{{ $payment->route }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Deskripsi :</strong></label>
                        <div class="card col-md-8 p-0" style="background-color: #F7F7F7">
                            <div class="card-body">
                                <label class="col-md-12 s-12">{!! $payment->ket !!}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Icon :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src') . $path . $payment->icon }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection