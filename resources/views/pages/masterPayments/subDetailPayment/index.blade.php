@extends('layouts.app')
@section('title', '| Data Sub Detail Payment')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon icon icon-payment"></i>
                        List {{ $title }}
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="row">
            <div class="col-md-8">
                <div class="card no-b">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="payment_id" class="col-form-label s-12 col-md-3 text-right"><strong>Payment :</strong></label>
                            <div class="col-sm-6">
                                <select name="payment_id" id="payment_id" class="select2 form-control r-0 light s-12" onchange="selectOnChange()">
                                    <option value="">Semua</option>
                                    @foreach ($payment as $i)
                                    <option value="{{ $i->id }}">{{ $i->n_payment }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group row" style="margin-top: -10px">
                            <label for="sub_payment_id" class="col-form-label s-12 col-md-3 text-right"><strong>Sub Payment :</strong></label>
                            <div class="col-sm-6">
                                <select name="sub_payment_id" id="sub_payment_id" class="select2 form-control r-0 light s-12" onchange="selectOnChange()">
                                </select>
                            </div>
                        </div> 
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <th width="30">No</th>
                                    <th>Nama</th>
                                    <th>Sub Payment</th>
                                    <th>No Rekening</th>
                                    <th width="60"></th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="alert"></div>
                <div class="card no-b">
                    <div class="card-body">
                        <form class="needs-validation" id="form" method="POST" novalidate>
                            {{ method_field('POST') }}
                            <input type="hidden" id="id" name="id"/>
                            <h4 id="formTitle">Tambah Data</h4><hr>
                            <div class="form-row form-inline">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label s-12 col-md-4">Payment</label>
                                        <div class="col-md-8 p-0 bg-light">
                                            <select class="select2 form-control r-0 light s-12" name="tmpayment_sub_id" id="tmpayment_sub_id" autocomplete="off">
                                                <option value="">Pilih</option>
                                                @foreach ($subPayment as $i)
                                                    <option value="{{ $i->id }}">{{ $i->n_payment_sub }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-t-5">
                                        <label for="n_payment_sub_detail" class="col-form-label s-12 col-md-4">Nama</label>
                                        <input type="text" name="n_payment_sub_detail" id="n_payment_sub_detail" placeholder="" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                    </div>
                                    <div class="form-group m-0">
                                        <label for="no_rek" class="col-form-label s-12 col-md-4">No Rekening</label>
                                        <input type="text" name="no_rek" id="no_rek" placeholder="" class="form-control r-0 light s-12 col-md-8" autocomplete="off" required/>
                                    </div>
                                    <div class="form-group m-0 mt-1">
                                        <label for="ket" class="col-form-label s-12 col-md-4">Keterangan</label>
                                        <textarea name="ket" id="ket" class="form-control r-0 light s-12 col-md-8 tinyMCE5" autocomplete="off"></textarea>
                                    </div>
                                    <div class="form-group mt-2">
                                        <div class="col-md-4"></div>
                                        <button type="submit" class="btn btn-primary btn-sm" id="action"><i class="icon-save mr-2"></i>Simpan<span id="txtAction"></span></button>
                                        <a class="btn btn-sm" onclick="add()" id="reset">Reset</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table = $('#dataTable').dataTable({
        processing: true,
        serverSide: true,
        order: [ 0, 'asc' ],
        ajax: {
            url: "{{ route($route.'api') }}",
            method: 'POST',
            data: function (data) {
                data.payment_id = $('#payment_id').val();
                data.sub_payment_id = $('#sub_payment_id').val();
            }
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false, align: 'center', className: 'text-center'},
            {data: 'n_payment_sub_detail', name: 'n_payment_sub_detail'},
            {data: 'tmpayment_sub_id', name: 'tmpayment_sub_id'},
            {data: 'no_rek', name: 'no_rek'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    });

    function selectOnChange(){
        table.api().ajax.reload();
    }

    $('#payment_id').on('change', function(){
        val = $(this).val();
        option = "<option value=''>&nbsp;</option>";
        if(val == ""){
            $('#sub_payment_id').html(option);
            selectOnChange();
        }else{
            $('#sub_payment_id').html("<option value=''>Loading...</option>");
            url = "{{ route($route.'subPaymentByPayment', ':id') }}".replace(':id', val);
            $.get(url, function(data){
                if(data){
                    $.each(data, function(index, value){
                        option += "<option value='" + value.id + "'>" + value.n_payment_sub +"</li>";
                    });
                    $('#sub_payment_id').empty().html(option);

                    $("#sub_payment_id").val($("#sub_payment_id option:first").val()).trigger("change.select2");
                }else{
                    $('#sub_payment_id').html(option);
                    selectOnChange();
                }
            }, 'JSON');
        }
        $('#sub_payment_id').change();
    });

    function add(){
        save_method = "add";
        $('#form').trigger('reset');
        $('#formTitle').html('Tambah Data');
        $('input[name=_method]').val('POST');
        $('#txtAction').html('');
        $('#reset').show();
        $('#tmpayment_sub_id').val("");
        $('#tmpayment_sub_id').trigger('change.select2');
        $('#n_payment_sub_detail').focus();
    }

    add();
    $('#form').on('submit', function (e) {
        if ($(this)[0].checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else{
            $('#alert').html('');
            $('#ket').val(tinymce.get("ket").getContent());
            $('#action').attr('disabled', true);
            url = (save_method == 'add') ? "{{ route($route.'store') }}" : "{{ route($route.'update', ':id') }}".replace(':id', $('#id').val());
            $.post(url, $(this).serialize(), function(data){
                $('#alert').html("<div role='alert' class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Success!</strong> " + data.message + "</div>");
                table.api().ajax.reload();
                if(save_method == 'add') add();
            }, "JSON").fail(function(data){
                err = ''; respon = data.responseJSON;
                $.each(respon.errors, function(index, value){
                    err += "<li>" + value +"</li>";
                });
                $('#alert').html("<div role='alert' class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Error!</strong> " + respon.message + "<ol class='pl-3 m-0'>" + err + "</ol></div>");
            }).always(function(){
                $('#action').removeAttr('disabled');
            });
            return false;
        }
        $(this).addClass('was-validated');
    });

    function edit(id) {
        save_method = 'edit';
        var id = id;
        $('#alert').html('');
        $('#form').trigger('reset');
        $('#formTitle').html("Edit Data <a href='#' onclick='add()' class='btn btn-outline-danger btn-xs pull-right ml-2'>Batal</a>");
        $('#txtAction').html(" Perubahan");
        $('#reset').hide();
        $('input[name=_method]').val('PATCH');
        $.get("{{ route($route.'edit', ':id') }}".replace(':id', id), function(data){
            $('#id').val(data.id);
            $('#tmpayment_sub_id').val(data.tmpayment_sub_id);
            $('#tmpayment_sub_id').trigger('change.select2');
            $('#n_payment_sub_detail').val(data.n_payment_sub_detail).focus();
            $('#no_rek').val(data.no_rek);
            tinymce.activeEditor.setContent(data.ket);
        }, "JSON").fail(function(){
            reload();
        });
    }

    function remove(id){
        $.confirm({
            title: '',
            content: 'Apakah Anda yakin akan menghapus data ini ?',
            icon: 'icon icon-question amber-text',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'red',
            buttons: {
                ok: {
                    text: "ok!",
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: function(){
                        $.post("{{ route($route.'destroy', ':id') }}".replace(':id', id), {'_method' : 'DELETE'}, function(data) {
                           table.api().ajax.reload();
                            if(id == $('#id').val()) add();
                        }, "JSON").fail(function(){
                            reload();
                        });
                    }
                },
                cancel: function(){}
            }
        });
    }

</script>
@endsection
