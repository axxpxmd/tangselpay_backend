@extends('layouts.app')
@section('title', '| Data User')
@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row">
                <div class="col">
                    <h4>
                        <i class="icon icon-user"></i>
                        Show {{ $title }} | {{ $pemilik->nama }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pegawai-tab" role="tablist">
                    <li>
                        <a class="nav-link" href="{{ route($route.'index') }}"><i class="icon icon-arrow_back"></i>Semua Data</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="card">
            <h6 class="card-header"><strong>Pemilik</strong></h6>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->nama }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Nama Perusahaan :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->nm_perusahaan }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>No Telpon :</strong></label>
                        <label class="col-md-3 s-12">{{ $pemilik->no_tlpn }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Alamat :</strong></label>
                        <label class="col-md-5 s-12">{{ $pemilik->alamat }}</label>
                    </div>
                    <div class="row">
                        <label class="col-md-2 text-right s-12"><strong>Logo  :</strong></label>
                        <img class="ml-2 m-t-7" src="{{ config('app.sftp_src').'assets/icon/logo/'.$pemilik->logo }}" width="100" alt="icon">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card mt-2">
                    <h6 class="card-header"><strong>Data User</strong></h6>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Username :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->username }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Versi API :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->api_version }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device ID :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_id }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>E-Money Status :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->emoney_status }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device Latitude :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_latitude }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device Longitude :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_longitude }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device Tipe :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_type }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device OS Version :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_os_version }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Device Platform :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->device_platform }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Package Version :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->package_version }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Package Build Number :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->package_build_number }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Tanggal Upgrade :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->tgl_upgrade }}</label>
                            </div>
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Version :</strong></label>
                                <label class="col-md-3 s-12">{{ $user->version }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mt-2">
                    <h6 class="card-header"><strong>Data User Detail</strong></h6>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Nama Depan :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->first_name }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Nama Belakang :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->last_name }}</label>
                            </div>  
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Nama :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->full_name }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Email :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->email }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>NIK :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->nik }}</label>
                            </div>  
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Telpon :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->telp }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Jenis Kelamin :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan' }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Tempat Lahir :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->t_lahir }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Tanggal Lahir :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->d_lahir }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Pekerjaan :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->pekerjaan }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Kelurahan :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->kelurahan_r != null ? $userDetail->kelurahan_r->n_kelurahan : '-' }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Alamat :</strong></label>
                                <label class="col-md-3 s-12">{{ $userDetail->alamat }}</label>
                            </div> 
                            <div class="row">
                                <label class="col-md-4 text-right s-12"><strong>Logo  :</strong></label>
                                <img class="ml-2 m-t-7 rounded-circle" src="{{ config('app.sftp_src').$path.$userDetail->id.'/'.$userDetail->foto }}" width="100" alt="icon">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection