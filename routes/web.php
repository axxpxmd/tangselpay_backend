<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard2', 'DashboardController@index')->name('dashboard2');

Route::group(['middleware' => ['auth']], function () {

    /**
     * Master Role
     */
    Route::prefix('master-roles')->namespace('MasterRole')->name('master-role.')->group(function () {
        // Permission
        Route::resource('permission', 'PermissionController');
        Route::post('permission/api', 'PermissionController@api')->name('permission.api');
        // Role
        Route::resource('role', 'RoleController');
        Route::prefix('role')->name('role.')->group(function () {
            Route::post('api', 'RoleController@api')->name('api');
            Route::get('{id}/addPermissions', 'RoleController@permission')->name('addPermissions');
            Route::post('storePermissions', 'RoleController@storePermission')->name('storePermissions');
            Route::get('{id}/getPermissions', 'RoleController@getPermissions')->name('getPermissions');
            Route::delete('{name}/destroyPermission', 'RoleController@destroyPermission')->name('destroyPermission');
        });
        // Pengguna
        Route::resource('pengguna', 'PenggunaController');
        Route::prefix('pengguna')->name('pengguna.')->group(function () {
            Route::post('api', 'PenggunaController@api')->name('api');
            Route::get('kabupatenByProvinsi/{id}', 'PenggunaController@kabupatenByProvinsi')->name('kabupatenByProvinsi');
            Route::get('kecamatanByKabupaten/{id}', 'PenggunaController@kecamatanByKabupaten')->name('kecamatanByKabupaten');
            Route::get('kelurahanByKecamatan/{id}', 'PenggunaController@kelurahanByKecamatan')->name('kelurahanByKecamatan');
            Route::get('editPassword/{id}', 'PenggunaController@editPassword')->name('editPassword');
            Route::post('updatePassword/{id}', 'PenggunaController@updatePassword')->name('updatePassword');
        });
    });

    /**
     * Master Pemilik
     */
    Route::prefix('master-pemiliks')->namespace('MasterPemilik')->name('master-pemilik.')->group(function () {
        // Pemilik
        Route::resource('pemilik', 'PemilikController');
        Route::post('pemilik/api', 'PemilikController@api')->name('pemilik.api');
        // Pemilik Ketentuan
        Route::resource('pemilikKetentuan', 'PemilikKetentuanController');
        Route::post('pemilikKetentuan/api', 'PemilikKetentuanController@api')->name('pemilikKetentuan.api');
    });

    /**
     * Master Config
     */
    Route::prefix('master-configs')->namespace('MasterConfig')->name('master-config.')->group(function () {
        // App
        Route::resource('app', 'AppController');
        Route::post('app/api', 'AppController@api')->name('app.api');
        // Home
        Route::resource('home', 'CHomeController');
        Route::post('home/api', 'CHomeController@api')->name('home.api');
        // Auth
        Route::resource('auth', 'AuthController');
        Route::post('auth/api', 'AuthController@api')->name('auth.api');
        // Theme
        Route::resource('theme', 'ThemeController');
        Route::post('theme/api', 'ThemeController@api')->name('theme.api');
        // Splash
        Route::resource('splash', 'SplashController');
        Route::post('splash/api', 'SplashController@api')->name('splash.api');
    });

    /**
     * Master Merchant
     */
    Route::prefix('master-merchants')->namespace('MasterMerchant')->name('master-merchant.')->group(function () {
        // Merchant
        Route::resource('merchant', 'MerchantController');
        Route::post('merchant/api', 'MerchantController@api')->name('merchant.api');
        // Jenis Merchant
        Route::resource('jenisMerchant', 'JenisMerchantController');
        Route::post('jenisMerchant/api', 'JenisMerchantController@api')->name('jenisMerchant.api');
    });

    /**
     * Master Payment
     */
    Route::prefix('master-payments')->namespace('MasterPayment')->name('master-payment.')->group(function () {
        // Payment
        Route::resource('payment', 'PaymentController');
        Route::post('payment/api', 'PaymentController@api')->name('payment.api');
        // Sub Payment
        Route::resource('subPayment', 'SubPaymentController');
        Route::post('subPayment/api', 'SubPaymentController@api')->name('subPayment.api');
        // Sub Detail Payment
        Route::resource('subDetailPayment', 'SubDetailPaymentController');
        Route::post('subDetailPayment/api', 'SubDetailPaymentController@api')->name('subDetailPayment.api');
        Route::get('subDetailPayment/subPaymentByPayment/{id}', 'SubDetailPaymentController@subPaymentByPayment')->name('subDetailPayment.subPaymentByPayment');
    });

    /**
     * Page Not Found
     */
    Route::get('blank-page', 'BlankPageController@index')->name('blank-page');

    /**
     * Master Media
     */
    Route::prefix('master-media')->namespace('MasterMedia')->name('master-media.')->group(function () {
        // Iklan
        Route::resource('iklan', 'IklanController');
        Route::post('iklan/api', 'IklanController@api')->name('iklan.api');
        // Berita
        Route::resource('berita', 'BeritaController');
        Route::post('berita/api', 'BeritaController@api')->name('berita.api');
    });

    /**
     * Master User
     */
    Route::prefix('master-user')->namespace('MasterUser')->name('master-user.')->group(function () {
        // User
        Route::resource('user', 'UserController');
        Route::post('user/api', 'UserController@api')->name('user.api');
    });

    /**
     * Master Transaksi
     */
    Route::prefix('master-transaksi')->namespace('MasterTransaksi')->name('master-transaksi.')->group(function () {
        // Transaksi
        Route::resource('transaksi', 'TransaksiController');
        Route::post('transaksi/api', 'TransaksiController@api')->name('transaksi.api');
    });

    Route::prefix('logviewer')->namespace('LogViewer')->name('logviewer.')->group(function () {
        Route::resource('logs', 'LogsController');
    });
});
